#!/usr/bin/env python3

from major.PyMajor import PyMajor
from major.PyAyla import PyAyla
import os
import wget
import argparse

debug = 0

def debugprint(instr):
    if debug == 1:
        print(instr)
    
parser = argparse.ArgumentParser()
group = parser.add_mutually_exclusive_group(required=True)
group.add_argument('--MAC', default=None)
group.add_argument('--DSN', default=None)
parser.add_argument('--property', required=True)
parser.add_argument('--email', default=argparse.SUPPRESS)
parser.add_argument('--password', default=argparse.SUPPRESS)
args = parser.parse_args()

aylainfo = PyAyla(args)
mac = args.MAC
dsn = args.DSN
propertyName = args.property

if (mac or dsn):

    pymajorClient = PyMajor(aylainfo.env, aylainfo.login_email, aylainfo.login_password)
    if debug == 1:
        if pymajorClient:
            print('Client is instantiated for property datapoints')

    mydevice = {}
    details = {}
    allproperties = {}
    if mac:
       mydevice = pymajorClient.get_device_byMAC(mac)
       if mydevice['total'] == 0:
           print('Unknown MAC: \'{}\''.format(mac))
           exit(1)
       dsn = mydevice['devices'][0]['device']['dsn']
    
    if dsn:
       details =  pymajorClient.get_dsn_details(dsn)
       allproperties = pymajorClient.get_properties(details['id'])

    # Initalize some stuff
    myproperties = {}
    for count,each in enumerate(allproperties):
        myproperties[each['property']['name']] = each['property']

    try:
        datapoints =  pymajorClient.get_property_datapoints(myproperties[propertyName]['key'])
    except KeyError as e:
        print('Property \'{}\' not found'.format(propertyName))
        exit(1)

    for count,each in reversed(list(enumerate(datapoints))):
        mydir = dsn+'/'+propertyName
        try:
            awsfile = each['datapoint']['file']
            # strip everything away so we can test if we already have this file
            myfile = awsfile.split('/',2)[2]
            myfile = myfile.split('?',1)[0]
            myfile = myfile.split('/',1)[1]
            try:
                os.makedirs(mydir)
            except FileExistsError as f:
                pass
            if os.path.isfile(os.path.join(mydir,myfile)):
                print('{} already exists... skipping'.format(myfile))
            else:
                print(awsfile)
                datafile = wget.download(awsfile,out=mydir)
        except KeyError as e:
            print('[{}] Created: {} = {}'.format(count,each['datapoint']['created_at'],each['datapoint']['value']))
            pass
    exit()
    
else:
    parser.print_help()
