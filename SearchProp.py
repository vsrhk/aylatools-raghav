#!/usr/bin/env python3

from major.PyMajor import PyMajor
from major.PyAyla import PyAyla
import os, sys
import time
import datetime
import argparse

debug = 0

def debugprint(instr):
    if debug == 1:
        print(instr)
    
def snippet(myin,myout):
    debugprint(myin)
    return myout+myin+'\n'
    
parser = argparse.ArgumentParser()
parser.add_argument('--oldest', default="2019-09-06+10:00:00")
parser.add_argument('--imageV', default=None)
parser.add_argument('--imageM', default=None)
parser.add_argument('--wait', type=int, default=60)
parser.add_argument('--email', default=argparse.SUPPRESS)
parser.add_argument('--password', default=argparse.SUPPRESS)
args = parser.parse_args()

aylainfo = PyAyla(args)
oldest = args.oldest
imageV = args.imageV
imageM = args.imageM
waittime = args.wait

# Initalize some stuff
mydevices = {}
myproperties = {}
eachprop = {}
theimageV = {}
theimageM = {}

message = ''

pymajorClient = PyMajor(aylainfo.env, aylainfo.login_email, aylainfo.login_password)
if debug == 1:
    if pymajorClient:
        print('Client is instantiated for Search devices')

status = 0
offline = 0
if aylainfo.env == 'field':
    search1 = 'RV1000'
    search2 = 'RV1000A'
else:
    search1 = 'RV2019QF'
    search2 = 'RV2019QFA'

alldevices1 = pymajorClient.get_device_bySearch('SET_Enable_RT_Map',1,search1)
start = 0
if alldevices1:
    for count, each in enumerate(alldevices1['devices'], start):
        if each:
            mydevices[count] = each['device']
            start = count

alldevices2 = pymajorClient.get_device_bySearch('SET_Enable_RT_Map',1,search2)
if alldevices2:
    for count, each in enumerate(alldevices2['devices'], start):
        if each:
            mydevices[count] = each['device']
            start = count

# loop thru all the devices and grab the 'right' ones
print('{},{},{},{},{}'.format('Name','email','MAC','status','lastconnected'))
for key, value in mydevices.items():
    if value['user_id'] != None:
        myuser = pymajorClient.get_user_details(value['user_id'])
        if myuser:
            print('{},{},{},{},{}'.format(value['product_name'],myuser['email'],value['mac'],value['connection_status'],value['connected_at']))
        else:
            print('{},{},{},{},{}'.format(value['product_name'],'None',value['mac'],value['connection_status'],value['connected_at']))
    else:
        print('{},{},{},{},{}'.format(value['product_name'],'None',value['mac'],value['connection_status'],value['connected_at']))

exit()
