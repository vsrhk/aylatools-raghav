#!/usr/bin/env python3

from major.PyMajor import PyMajor
from major.PyAyla import PyAyla
import os,sys
import time
import datetime
import argparse

debug = 0
jobsstarted = 0

def debugprint(instr):
    if debug == 1:
        print(instr)
    
def snippet(myin,myout):
    debugprint(myin)
    return myout+myin+'\n'
    

parser = argparse.ArgumentParser()
parser.add_argument('--filtered')
parser.add_argument('--name')
parser.add_argument('--email', default=argparse.SUPPRESS)
parser.add_argument('--password', default=argparse.SUPPRESS)
args = parser.parse_args()

aylainfo = PyAyla(args)
filterstr = args.filtered
namestr = args.name

pymajorClient = PyMajor(aylainfo.env, aylainfo.login_email, aylainfo.login_password)
if pymajorClient:
    print('Client is instantiated for CheckJobs')

message = ''
    
myjobs = {}
otajobs = {}
alljobs = pymajorClient.get_ota_jobs('started',namestr)
if not alljobs:
    print('No jobs returned')
else:
    print('Total jobs: {}'.format(alljobs[1]['total']))
    exit()

for count, jobs in enumerate(otajobs):
    myjobs[jobs['job']['name']] = jobs['job']

myjobsdetails = {}
for key, value in myjobs.items():
    myjobsdetails[value['id']] = pymajorClient.get_job_detail(value['id'])

for key, value in myjobs.items():
    if name:
        if key == name:
            print('Job Name: {} - Status: {}, Device Count: {}, In Progress: {}, Failed: {}, Success: {}'.format(key,myjobsdetails[value['id']]['status'],myjobsdetails[value['id']]['device_count'],myjobsdetails[value['id']]['in_progress_count'],myjobsdetails[value['id']]['failed_count'],myjobsdetails[value['id']]['succeed_count']))
            myprogress = pymajorClient.get_job_devices(value['id'])
            for count, each in enumerate(myprogress):
                if each['status'] == 'in_progress':
                    details = pymajorClient.get_dsn_details(each['dsn'])
                    lastconnect = pymajorClient.get_last_connection(details['id'])
                    properties = pymajorClient.get_properties(details['id'])
                    theproperties = {}
                    for acount, aeach in enumerate(properties):
                        theproperties[aeach['property']['name']] = aeach['property']
                        print('\tMAC: {}, Name: \'{}\''.format(details['mac'],each['product_name']))
                        print('\t\tConnection status {} at {}'.format(lastconnect['status'],lastconnect['event_time']))
                        try:
                            print('\t\tRobot Firmware Version: {}'.format(theproperties['GET_Robot_Firmware_Version']['value']))
                        except KeyError as e:
                            print('\t\tRobot Firmware Version: {}'.format('Not available'))
    else:
        print('Job Name: {} - Status: {}, Device Count: {}, In Progress: {}, Failed: {}, Success: {}'.format(key,myjobsdetails[value['id']]['status'],myjobsdetails[value['id']]['device_count'],myjobsdetails[value['id']]['in_progress_count'],myjobsdetails[value['id']]['failed_count'],myjobsdetails[value['id']]['succeed_count']))

exit()
