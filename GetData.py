#!/usr/bin/env python3

from major.PyMajor import PyMajor
from major.PyAyla import PyAyla
import os,sys
import time, datetime
import pickle
import argparse
from pathlib import Path

debug = 0

def debugprint(instr):
    if debug == 1:
        print(instr)
    
def snippet(myin,myout):
    debugprint(myin)
    return myout+myin+'\n'


parser = argparse.ArgumentParser()
parser.add_argument('--update', action='store_true')
parser.add_argument('--email', default=argparse.SUPPRESS)
parser.add_argument('--password', default=argparse.SUPPRESS)
args = parser.parse_args()

aylainfo = PyAyla(args)
update = args.update

pymajorClient = PyMajor(aylainfo.env, aylainfo.login_email, aylainfo.login_password)
if pymajorClient:
    print('Client is instantiated')

xdevices = {}
mydevices = {}
myproperties = {}

if os.path.isfile('mydevices.pickle'):
    recent = datetime.datetime.fromtimestamp(os.path.getmtime('mydevices.pickle'))
    print('Last update: {}'.format(recent.strftime("%Y-%m-%d+%H:%M:%S")))
    now = datetime.datetime.now()
    print('Now: {}'.format(now.strftime("%Y-%m-%d+%H:%M:%S")))

if not update or not os.path.isfile('mydevices.pickle'):
    print('Getting all')
    alldevices =  pymajorClient.get_all_devices()
else:
    print('Loading saved mydevices...')
    with open('mydevices.pickle', 'rb') as f:
        mydevices = pickle.load(f)

    print('Getting new')
    alldevices =  pymajorClient.get_new_devices(recent)

if alldevices:
    print('Total devices: {}'.format(alldevices[0]['total']))

start = 0
for each in alldevices:
    if each:
        for count, details in enumerate(each['devices'], start):
            debugprint('{} {}'.format(count,details))
            xdevices[count] = details
        start = each['end_count_on_page']

# Initalize some stuff
allproperties = {}
for count, each in xdevices.items():
    # sleep for 300ms to prevent 429 errors
    time.sleep(.3)
    print('.', end='', flush=True)
    debugprint(each['device']['dsn'])
    mydetails = pymajorClient.get_dsn_details(each['device']['dsn'])
    if mydetails:
        mydevices[mydetails['id']] = mydetails
try:
    os.remove("mydevices.pickle")
except OSError:
    pass

print('\nGetting offline info', end = '')
for key, value in mydevices.items():
    # sleep for 300ms to prevent 429 errors
    time.sleep(.3)
    print('.', end='', flush=True)
    try:
        if value['connection_status'] == 'Offline':
            lastconnect =  pymajorClient.get_last_connection(value['id'])
            if lastconnect:
                value['offline'] = lastconnect['event_time']
    except KeyError as e:
        pass

print('\nDumping mydevices...')
with open('mydevices.pickle', 'wb') as f:
    # Pickle the 'data' dictionary using the highest protocol available.
    pickle.dump(mydevices, f, pickle.HIGHEST_PROTOCOL)
    
print('\nGetting properties', end='')
for key, value in mydevices.items():
    # sleep for 300ms to prevent 429 errors
    time.sleep(.3)
    print ('.', end='', flush=True)
    allproperties = pymajorClient.get_properties(value['id'])
    debugprint(allproperties)
    # for each device save the all properties indexed by ['id']['<property name>']
    if allproperties:
        for count,each in enumerate(allproperties):
            myproperties[value['id'],each['property']['name']] = each['property']

try:
    os.remove("myproperties.pickle")
except OSError:
    pass

print('\nDumping myproperties...')
with open('myproperties.pickle', 'wb') as f:
    # Pickle the 'data' dictionary using the highest protocol available.
    pickle.dump(myproperties, f, pickle.HIGHEST_PROTOCOL)
allusers = {}
print('\nGetting users', end='')
for key, value in mydevices.items():
    userID = value['user_id']
    if isinstance(userID, (int)):
        try:
            if allusers[userID]:
                print ('', end='')
        except KeyError as e:
            allusers[userID] = pymajorClient.get_user_details(userID)
            # sleep for 100ms to prevent 429 errors
            time.sleep(.1)
            pass
        print ('.', end='', flush=True)

try:
    os.remove("myusers.pickle")
except OSError:
    pass

print('\nDumping users...')
with open('myusers.pickle', 'wb') as f:
    # Pickle the 'data' dictionary using the highest protocol available.
    pickle.dump(allusers, f, pickle.HIGHEST_PROTOCOL)

exit()
