#!/usr/bin/env python3

import base64
import os
import getpass
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.fernet import Fernet

password_provided = getpass.getuser() # This is input in the form of a string
password = password_provided.encode() # Convert to type bytes
kdf = PBKDF2HMAC(
    algorithm=hashes.SHA256(),
    length=32,
    salt=password,
    iterations=100000,
    backend=default_backend()
)
key = base64.urlsafe_b64encode(kdf.derive(password)) # Can only use kdf once
# print(key)

message = input('Password: ').encode()
f = Fernet(key)
encrypted = f.encrypt(message)
print('Your encrypted key: {}'.format(encrypted.decode()))

decrypted = f.decrypt(encrypted)
# print(decrypted.decode())
