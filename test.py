import datetime
import time
import numpy as np 
import calendar


flag = {
    'justified' : False,
    'paused'    : False,
    'cleaning'  : False,
    'docking'   : False,
    'docked'    : False,
    'error'     : False,
    'redock'    : False,
    'mission'   : 0
}

Error = {
    0 : 'alarmNone',
    1 : 'alarmReserved',
    2 : 'alarmSideBrush',
    3 : 'alarmFan',
    4 : 'alarmMainBrush',
    5 : 'alarmWheels',
    6 : 'alarmBumper',
    7 : 'alarmCliffSensor',
    8 : 'alarmNoBatteryOrFuse',
    9 : 'alarmNoDustBox',
    10 : 'alarmDrop',
    11 : 'alarmFrontWheelStuck',
    12 : 'alarmCharger',
    13 : 'alarmReserved1',
    14 : 'alarmMagStrip',
    15 : 'alarmReserved2',
    16 : 'alarmTopBumper',
    17 : 'alarmBatteryLife',
    18 : 'alarmWheelEncoder',
    19 : 'alarmAccelerometer',
    20 : 'alarmMainBrush2',
    21 : 'alarmNav',
    22 : 'alarmUL'
}
def som():
    return True, 5


def setflags(true_list, value):
    for key in true_list:
        try:
            flag[key]=value
        except:
            debugprint("The flag {} is not part of the dictionary!".format(key))
            sprint("The flag {} is not part of the dictionary!".format(key))
    return True

def midnight(e):
    d = time.gmtime(e)
    d = time.strftime('%Y-%m-%d ', d) + "00:00:00"
    d = time.mktime(time.strptime(d, "%Y-%m-%d %H:%M:%S"))
    return int(d)



print(time.asctime(time.localtime()))
a = np.asarray([12, 32])

print(a.size)


t = 1576213200
val = time.strftime("%z")[-4:]
timeshift = int(val[0:2])*60*60

print(t)
print(timeshift)
print(t-timeshift)
print(time.localtime(t))

#print(t-midnight(1576213300-timeshift))
#print(time.strftime("%a, %d %b %Y %H:%M:%S %Z", time.localtime(t)))
