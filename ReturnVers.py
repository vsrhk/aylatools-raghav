#!/usr/bin/env python3

import sys
import time
import datetime
import pickle
from pprint import pprint

debug = 0

def debugprint(instr):
    if debug == 1:
        print(instr)
    
def snippet(myin,myout):
    debugprint(myin)
    return myout+myin+'\n'
    
if len(sys.argv) != 1:
    print('usage:  python3 ReturnVers.py\n')
else:

    # Initalize some stuff
    mydevices = {}
    myproperties = {}

    print('Loading mydevices...')
    with open('mydevices.pickle', 'rb') as f:
        mydevices = pickle.load(f)

    print('Loading myproperties...')
    with open('myproperties.pickle', 'rb') as f:
        myproperties = pickle.load(f)

    print('Loading myuser...')
    with open('myusers.pickle', 'rb') as f:
        myusers = pickle.load(f)

    # loop thru all the devices and extract OTA_FW_VERSION
    for key, value in mydevices.items():
        try:
            print('---------------------------')
            print('{} Properties:'.format(key))
        except KeyError as e:
            pass

        try:
            print('Model: {} OEM Model: {}'.format(value['model'],value['oem_model']))
        except KeyError as e:
            print('Model: {} OEM Model: {}'.format('Not available','Not available'))
            pass
            
        if value['connection_status'] != 'Online':
            try:
                print('\tConnection History: Offline at {}'.format(value['offline']))
            except KeyError as e:
                print('\tConnection History: {}'.format('Not available'))
                pass

            try:
                print('\tLast Connected: {}'.format(value['last_get_at']))
            except KeyError as e:
                print('\tLast Connected: {}'.format('Not available'))
                pass

            try:
                print('\tExtended Error Code: {}'.format(myproperties[value['id'],'GET_Extended_Error_Code']['value']))
            except KeyError as e:
                print('\tExtended Error Code: {}'.format('Not available'))
                pass

            try:
                print('\tError Code: {}'.format(myproperties[value['id'],'GET_Error_Code']['value']))
            except KeyError as e:
                print('\tError Code: {}'.format('Not available'))
                pass

            try:
                print('\tBattery Capacity: {}'.format(myproperties[value['id'],'GET_Battery_Capacity']['value']))
            except KeyError as e:
                print('\tBattery Capacity: {}'.format('Not available'))
                pass

            try:
                print('\tCharging Status: {}'.format(myproperties[value['id'],'GET_Charging_Status']['value']))
            except KeyError as e:
                print('\tCharging Status: {}'.format('Not available'))
                pass

            try:
                print('\tGet Operating Mode: {}'.format(myproperties[value['id'],'GET_Operating_Mode']['value']))
            except KeyError as e:
                print('\tGet Operating Mode: {}'.format('Not available'))
                pass

            try:
                print('\tSet Operation Mode: {}'.format(myproperties[value['id'],'SET_Operating_Mode']['value']))
            except KeyError as e:
                print('\tSet Operation Mode: {}'.format('Not available'))
                pass                
        else:
            try:
                print('Connection Status: {}'.format(value['connection_status']))
            except KeyError as e:
                print('Connection Status: {}'.format('Not available'))
                pass

        try:
            print('MAC: {} (\'{}\', id:{})'.format(value['mac'],value['product_name'],value['id']))
        except KeyError as e:
            print('MAC: {} (\'{}\', id:{})'.format('Not available','Not available','Not available'))
            pass

        try:
            print('DSN: {}'.format(value['dsn']))
        except KeyError as e:
            print('DSN: {}'.format('Not available'))
            pass

        try:
            print('sw_version: \'{}\''.format(value['sw_version']))
        except KeyError as e:
            print('sw_version: \'{}\''.format('Not available'))
            pass

        try:
            print('Email ID: {}, Email address: \'{}\''.format(myusers[value['user_id']]['id'],myusers[value['user_id']]['email']))
        except KeyError as e:
            print('Email ID: {}, Email address: {}'.format('Not available','Not available'))

        try:
            print('GET_Main_PCB_BL_Version: \'{}\''.format(myproperties[value['id'],'GET_Main_PCB_BL_Version']['value']))
        except KeyError as e:
            print('GET_Main_PCB_BL_Version: \'{}\''.format('Not available'))
            pass

        try:
            print('GET_Main_PCB_FW_Version: \'{}\''.format(myproperties[value['id'],'GET_Main_PCB_FW_Version']['value']))
        except KeyError as e:
            print('GET_Main_PCB_FW_Version: \'{}\''.format('Not available'))
            pass

        try:
            print('GET_Main_PCB_HW_Version: \'{}\''.format(myproperties[value['id'],'GET_Main_PCB_HW_Version']['value']))
        except KeyError as e:
            print('GET_Main_PCB_HW_Version: \'{}\''.format('Not available'))
            pass

        try:
            print('GET_Nav_Module_App_Version: \'{}\''.format(myproperties[value['id'],'GET_Nav_Module_App_Version']['value']))
        except KeyError as e:
            print('GET_Nav_Module_App_Version: \'{}\''.format('Not available'))
            pass

        try:
            print('GET_Nav_Module_BL_Version: \'{}\''.format(myproperties[value['id'],'GET_Nav_Module_BL_Version']['value']))
        except KeyError as e:
            print('GET_Nav_Module_BL_Version: \'{}\''.format('Not available'))
            pass

        try:
            print('GET_Nav_Module_FW_Version: \'{}\''.format(myproperties[value['id'],'GET_Nav_Module_FW_Version']['value']))
        except KeyError as e:
            print('GET_Nav_Module_FW_Version: \'{}\''.format('Not available'))
            pass

        try:
            print('GET_Nav_Module_HW_Version: \'{}\''.format(myproperties[value['id'],'GET_Nav_Module_HW_Version']['value']))
        except KeyError as e:
            print('GET_Nav_Module_HW_Version: \'{}\''.format('Not available'))
            pass

        try:
            print('GET_Robot_Firmware_Version: \'{}\''.format(myproperties[value['id'],'GET_Robot_Firmware_Version']['value']))
        except KeyError as e:
            print('GET_Robot_Firmware_Version: \'{}\''.format('Not available'))
            pass

        try:
            print('GET_SCM_FW_Version: \'{}\''.format(myproperties[value['id'],'GET_SCM_FW_Version']['value']))
        except KeyError as e:
            print('GET_SCM_FW_Version: \'{}\''.format('Not available'))
            pass

        try:
            print('GET_UBD_Version: \'{}\''.format(myproperties[value['id'],'GET_UBD_Version']['value']))
        except KeyError as e:
            print('GET_UBD_Version: \'{}\''.format('Not available'))
            pass

        try:
            print('GET_WiFi_FW_Version: \'{}\''.format(myproperties[value['id'],'GET_WiFi_FW_Version']['value']))
        except KeyError as e:
            print('GET_WiFi_FW_Version: \'{}\''.format('Not available'))
            pass

        try:
            print('GET_WiFi_HW_Version: \'{}\''.format(myproperties[value['id'],'GET_WiFi_HW_Version']['value']))
        except KeyError as e:
            print('GET_WiFi_HW_Version: \'{}\''.format('Not available'))
            pass
    exit()
