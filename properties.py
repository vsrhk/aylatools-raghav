debug = 0
badcount = 0 

import os,sys

def debugprint(instr):
    global debug
    if debug == 1:
        print(instr)
    
def snippet(myin,myout):
    debugprint(myin)
    return myout+myin+'\n'

def getproperties(client, devices, props, status):
    global badcount
    device_msg = ''

    # search thru all the devices and record one with GET_Charging_Status = 0
    for key, value in devices.items():
        charging_status = 0
        try:
            charging_status = props[value['id'],'GET_Charging_Status']['value']
        except KeyError as e:
            pass

        connection_status = "Unset"
        try:
            connection_status = value['connection_status']
        except KeyError as e:
            pass

        try:
            lastconnect =  client.get_last_connection(value['id'])
        except KeyError as e:
            lastconnect['event_time'] = 0
            pass
            
        device_msg = snippet('\n',device_msg)
        if charging_status == 0 or connection_status == status:
            badcount += 1
            if connection_status != status:
                try:
                    response = os.system("/sbin/ping -t 2 -c 1 " + value['lan_ip'] + "> /dev/null")
                    #and then check the response...
                    if response == 0:
                        device_msg = snippet('LIAR! {} returns a ping'.format(value['lan_ip']),device_msg)
                except KeyError as e:
                    pass

        try:
            device_msg = snippet('\tConnection Status: {} at {}'.format(connection_status,lastconnect['event_time']),device_msg)
        except TypeError as e:
            pass

        try:
            device_msg = snippet('\tMAC: {} (\'{}\', id:{})'.format(value['mac'],value['product_name'],value['id']),device_msg)
        except KeyError as e:
            device_msg = snippet('\tMAC: {} (\'{}\', id:{})'.format('Not available','Not available','Not available'),device_msg)
            pass

        try:
            device_msg = snippet('\tDSN: {} '.format(value['dsn']),device_msg)
        except KeyError as e:
            device_msg = snippet('\tDSN: {} '.format('Not available'),device_msg)
            pass

        try:
            device_msg = snippet('\tOEM Model: {}'.format(value['oem_model']),device_msg)
        except KeyError as e:
            device_msg = snippet('\tOEM Model: {}'.format('Not available'),device_msg)
            pass

        try:
            device_msg = snippet('\tOTA FW Version: {}'.format(props[value['id'],'OTA_FW_VERSION']['value']),device_msg)
        except KeyError as e:
            device_msg = snippet('\tOTA FW Version: {}'.format('Not available'),device_msg)
            pass

        try:
            device_msg = snippet('\tWiFi FW Version: {}'.format(props[value['id'],'GET_WiFi_FW_Version']['value']),device_msg)
        except KeyError as e:
            device_msg = snippet('\tWiFi FW Version: {}'.format('Not available'),device_msg)
            pass

        try:
            device_msg = snippet('\tRobot Firmware Version: {}'.format(props[value['id'],'GET_Robot_Firmware_Version']['value']),device_msg)
        except KeyError as e:
            device_msg = snippet('\tRobot Firmware Version: {}'.format('Not available'),device_msg)
            pass

        try:
            device_msg = snippet('\tNav Module BL Version: {}'.format(props[value['id'],'GET_Nav_Module_BL_Version']['value']).rstrip('\n'),device_msg)
        except KeyError as e:
            device_msg = snippet('\tNav Module BL Version: {}'.format('Not available'),device_msg)
            pass

        try:
            device_msg = snippet('\tLast Connected: {}'.format(value['last_get_at']),device_msg)
        except KeyError as e:
            device_msg = snippet('\tLast Connected: {}'.format('Not available'),device_msg)
            pass

        try:
            device_msg = snippet('\tExtended Error Code: {}'.format(props[value['id'],'GET_Extended_Error_Code']['value']),device_msg)
        except KeyError as e:
            device_msg = snippet('\tExtended Error Code: {}'.format('Not available'),device_msg)
            pass

        try:
            device_msg = snippet('\tError Code: {}'.format(props[value['id'],'GET_Error_Code']['value']),device_msg)
        except KeyError as e:
            device_msg = snippet('\tError Code: {}'.format('Not available'),device_msg)
            pass

        try:
            device_msg = snippet('\tBattery Capacity: {}'.format(props[value['id'],'GET_Battery_Capacity']['value']),device_msg)
        except KeyError as e:
            device_msg = snippet('\tBattery Capacity: {}'.format('Not available'),device_msg)
            pass

        try:
            device_msg = snippet('\tCharging Status: {}'.format(props[value['id'],'GET_Charging_Status']['value']),device_msg)
        except KeyError as e:
            device_msg = snippet('\tCharging Status: {}'.format('Not available'),device_msg)
            pass

        try:
            device_msg = snippet('\tGet Operating Mode: {}'.format(props[value['id'],'GET_Operating_Mode']['value']),device_msg)
        except KeyError as e:
            device_msg = snippet('\tGet Operating Mode: {}'.format('Not available'),device_msg)
            pass

        try:
            device_msg = snippet('\tSet Operation Mode: {}'.format(props[value['id'],'SET_Operating_Mode']['value']),device_msg)
        except KeyError as e:
            device_msg = snippet('\tSet Operation Mode: {}'.format('Not available'),device_msg)
            pass

        jobstatus = client.get_job_status(value['dsn'])
        if jobstatus:
            device_msg = snippet('\tOTA Status: {} {}'.format(jobstatus['status'],jobstatus['job_name']),device_msg)

    return device_msg,badcount
