for foo in $(seq 1 155); do
    echo '----------' ${foo} '----------'
    # SCM60 - Mesa
    python3 OTAGroups.py --imageid 14042 8371 8372 8373 8375 8376 8378 8380 8381 8382 --goal S1.0.60 --wait 30
    if [ $? == 0 ]; then
	# SCM60 - Valley
	python3 OTAGroups.py --imageid 14060 8370 8377 8379 --goal S1.0.60 --wait 30
	if [ $? == 0 ]; then
	    # Full 54 - Mesa
	    python3 OTAGroups.py --imageid 14044 8371 8372 8373 8375 8376 8378 8380 8381 8382 --wait 30
	    if [ $? == 0 ]; then
		# Full 54 - Valley
		python3 OTAGroups.py --imageid 14045 8370 8377 8379 --wait 30
		if [ $? == 0 ]; then
		    echo 'Round' ${foo} 'Complete'
		fi
	    fi
	fi
    fi
    python3 ChargeCheck.py --mongroup MonitorGroup02
done
