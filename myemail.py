# Setup for email....
import smtplib
from email.message import EmailMessage

def send_myemail(sender, sender_passwd, receiver, subject, message):
    msg = EmailMessage()
    msg['From'] = sender
    msg['To'] = receiver
    msg['Subject'] = subject
    msg.set_content(message)

    try:
        server = smtplib.SMTP('smtp.office365.com',587)
        if not server:
            print("Server not defined")
            exit(1)
        foo = server.connect('smtp.office365.com',587)
        bar = server.starttls()
        server.ehlo()
        foobar = server.login(sender, sender_passwd)
        server.helo()
        server.send_message(msg)
        print("Successfully sent email")
        server.quit()
    except SMTPException:
        print("Error: unable to send email")

