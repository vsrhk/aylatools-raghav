#!/bin/bash 
/usr/bin/python3 GetData.py; /usr/bin/python3 ReturnVers.py > latest_vers.txt
cd /home/jschectman/pickles
MYDAY=`date +"%m-%d"`
mkdir $MYDAY
cd $MYDAY
cp /home/jschectman/aylatools/*.pickle .
cp /home/jschectman/aylatools/latest_vers.txt .
git add $MYDAY
git commit -m "updates for $MYDAY"
