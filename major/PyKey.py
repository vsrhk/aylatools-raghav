import base64
import os
import getpass
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.fernet import Fernet

class PyKey(object):

    def __init__(self, encrypted_login_password):
        self.in_the_clear = self.decrypt(encrypted_login_password)

    def decrypt(self, encrypted_login_password):
        
        password_provided = getpass.getuser() # This is input in the form of a string
        password = password_provided.encode() # Convert to type bytes
        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=32,
            salt=password,
            iterations=100000,
            backend=default_backend()
        )
        key = base64.urlsafe_b64encode(kdf.derive(password)) # Can only use kdf once

        f = Fernet(key)
        decrypted = f.decrypt(encrypted_login_password.encode())
        return decrypted.decode()

