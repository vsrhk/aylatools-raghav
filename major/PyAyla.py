from major.PyKey import PyKey
import os,sys
from pathlib import Path
import configparser

class PyAyla(object):

    def __init__(self, progargs):
        config = configparser.ConfigParser()
        configfile = os.path.join(str(Path.home()),'.aylaconfig')
        exists = os.path.isfile(configfile)
        self.env = os.environ.get('AYLAENV','dev')
        if exists:
            config.read(configfile)
            self.login_email = config.get(self.env,'ayla_email',fallback=None)
            
            self.smtp_email = config.get(self.env,'smtp_email',fallback=self.login_email)

            pyKey = PyKey(config.get(self.env,'ayla_password',fallback=None))
            self.login_password = pyKey.in_the_clear
            pyKey2 = PyKey(config.get(self.env,'smtp_password',fallback=config.get(self.env,'ayla_password')))
            self.smtp_password = pyKey2.in_the_clear
        else:
            try:
                if (vars(progargs)['email']):
                    self.login_email = vars(progargs)['email']
            except KeyError as e:
                pass
            try:
                if (vars(progargs)['password']):
                    self.login_password = vars(progargs)['password']
            except KeyError as e:
                pass
            
        if not (self.login_email and self.login_password):
            print('Missing ayla login and/or password')
            exit(1)
