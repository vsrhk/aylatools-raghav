import json
import logging
import time
import sys
import requests

logging.basicConfig(filename='major.log', level=logging.DEBUG)

URLS_dev = {
    'env' : 'env=us_developer',
    'app_id' : 'SDA-JQ-id',
    'app_secret' : 'SDA-rQOiX-fGHZRq88Fzq4Yb3nxQFLk',
    'host' : 'ads-dev.aylanetworks.com',
    'login' : 'https://ads-dev.aylanetworks.com',
    'devices' : 'https://ads-dev.aylanetworks.com',
    'search' : 'https://ads-devkube.aylanetworks.com',
    'images' : 'https://ais.aylanetworks.com',
    'users' : 'https://user-dev.aylanetworks.com',
    'dashboard' : 'https://dashboard-dev.aylanetworks.com',
    }

URLS_field = {
    'env' : 'env=field_production',
    'app_id' : 'Vac-iOS-dev-id',
    'app_secret' : 'Vac-iOS-dev-TFn4WRa14qNGae7eYh5M0HPSap0',
    'host' : 'ads.aylanetworks.com',
    'login':'https://ads.aylanetworks.com',
    'devices' : 'https://ads.aylanetworks.com',
    'search' : 'https://ads-field.aylanetworks.com',
    'images' : 'https://ais-field.aylanetworks.com',
    'users' : 'https://user.aylanetworks.com',
    'dashboard' : 'https://dashboard.aylanetworks.com',
    }

class PyMajor(object):
    """
    Class for making calls to ayla

    ...

    Attributes
    ----------
    auth_token = None
    expire_time = 0
    prop_expire_time = 0
    prop_ttl = prop_ttl
    app_active_expire = 0
    app_active_ttl = 5
    username = username
    password = password
    headers = None
    auth_header = None
    monitored_properties = []
    app_active_prop_id = None

    auth_token = self.login(username, password)

    Methods
    -------

    """

    def __init__(self, env, username, password, prop_ttl=15):
        self.auth_token = None
        self.expire_time = 0
        self.prop_expire_time = 0
        self.prop_ttl = prop_ttl
        self.app_active_expire = 0
        self.app_active_ttl = 5
        self.username = username
        self.password = password
        self.headers = None
        self.auth_header = None
        self.monitored_properties = []
        self.app_active_prop_id = None
        self.aylaenv = env
        if self.aylaenv == 'field':
            self.urls = URLS_field
        else:
            self.urls = URLS_dev            
        self.auth_token = self.login(username, password)

    def get_auth_header(self):
        '''
        Get the auth token. If the current token has not expired, return that.
        Otherwise login and get a new token and return that token.
        '''

        # if the auth token doesnt exist or has expired, login to get a new one
        if (self.auth_token is None) or (self.expire_time <= time.time()):
            logging.debug('Auth Token expired, need to get a new one')
            self.login(self.username, self.password)

        self.auth_header = {'content-type': 'application/json',
                            'accept': 'application/json',
                            'Authorization': 'auth_token ' + self.auth_token
                            }

        return self.auth_header

    def get_all_devices(self):
        page = 0
        json_data = [None] * 100
        allurl = '{}/apiv1/devices.json?ignore_limit=true&is_forward_page=false&match=true&order=desc&order_by=id&page={}&paginated=true&per_page=200'.format(self.urls['devices'],page+1)
        response = requests.get(allurl, headers=self.get_auth_header())
        if response.status_code == 200:
            json_data[page] = response.json()
        if json_data[page]['total'] == 0:
            return None
        while json_data[page]['total'] != json_data[page]['end_count_on_page']:
            page += 1
            allurl = '{}/apiv1/devices.json?ignore_limit=true&is_forward_page=false&match=true&order=desc&order_by=id&page={}&paginated=true&per_page=200'.format(self.urls['devices'],page+1)
            response = requests.get(allurl, headers=self.get_auth_header())
            if response.status_code == 200:
                json_data[page] = response.json()
        return json_data

    def get_device_byMAC(self, myfilter=None):
        json_data = None
        devurl = '{}/apiv1/devices.json?{}&[filter][mac]={}&ignore_limit=true&is_forward_page=false&match=true&order=desc&order_by=connected_at&page=1&paginated=true&per_page=10'.format(self.urls['devices'],self.urls['env'],myfilter)
        response = requests.get(devurl, headers=self.get_auth_header())
        if response.status_code == 200:
            json_data = response.json()
        return json_data

    def get_device_byActivation(self, myfilter=None, mymodel=None):
        if mymodel:
            oemmodel = '&oem_model={}'.format(mymodel)
        else:
            oemmodel = ''
        page = 0
        json_data = [None] * 100
        devurl = '{}/apiv1/devices.json?{}&[filter][activated_at_after]={}&ignore_limit=true&is_forward_page=false&match=true&order=desc&order_by=connected_at&page=1&paginated=true&per_page=200{}'.format(self.urls['devices'],self.urls['env'],myfilter,oemmodel)
        response = requests.get(devurl, headers=self.get_auth_header())
        if response.status_code == 200:
            json_data[page] = response.json()
        if json_data[page]['total'] == 0:
            return None
        while json_data[page]['total'] != json_data[page]['end_count_on_page']:
            page += 1
            allurl = '{}/apiv1/devices.json?{}&[filter][activated_at_after]={}&ignore_limit=true&is_forward_page=false&match=true&order=desc&order_by=connected_at&page={}&paginated=true&per_page=200{}'.format(self.urls['devices'],self.urls['env'],myfilter,page+1,oemmodel)
            response = requests.get(allurl, headers=self.get_auth_header())
            if response.status_code == 200:
                json_data[page] = response.json()
        return json_data

    def get_device_byModel(self, myfilter=None):
        page = 0
        json_data = [None] * 100
        devurl = '{}/apiv1/devices.json?{}&[filter][model]={}&ignore_limit=true&is_forward_page=false&match=true&order=desc&order_by=connected_at&page=1&paginated=true&per_page=200'.format(self.urls['devices'],self.urls['env'],myfilter)
        response = requests.get(devurl, headers=self.get_auth_header())
        if response.status_code == 200:
            json_data[page] = response.json()
        if json_data[page]['total'] == 0:
            return None
        while json_data[page]['total'] != json_data[page]['end_count_on_page']:
            page += 1
            allurl = '{}/apiv1/devices.json?{}&[filter][model]={}&ignore_limit=true&is_forward_page=false&match=true&order=desc&order_by=connected_at&page={}&paginated=true&per_page=200'.format(self.urls['devices'],self.urls['env'],myfilter,page+1)
            response = requests.get(allurl, headers=self.get_auth_header())
            if response.status_code == 200:
                json_data[page] = response.json()            
        return json_data

    def get_device_metadata(self, dsn=None):
        devurl = '{}/apiv1/dsns/{}/data?{}'.format(self.urls['search'],dsn,'env=docker')
        response = requests.get(devurl, headers=self.get_auth_header())
        if response.status_code == 200:
            if response.json():
                json_data = response.json()
            else:
                json_data = None
        return json_data

    def get_device_bySearch(self, myprop=None, myvalue=None, mymodel=None):
        if mymodel:
            oemmodel = '&oem_model={}'.format(mymodel)
        else:
            oemmodel = ''
        page = 0
        json_data = None
        devurl = '{}/apiv1/devices/search.json?{}&is_forward_page=false&match=true&oem_id=39a9391a{}&order_by=connected_at&page=1&paginated=true&per_page=100&property_name={}&search_by_property=true&value={}'.format(self.urls['search'],'env=docker',oemmodel,myprop,myvalue)
        response = requests.get(devurl, headers=self.get_auth_header())
        if response.status_code == 200:
            json_data = response.json()
        return json_data

    def get_new_devices(self, myfilter=None):
        page = 0
        json_data = [None] * 20
        allurl = '{}/apiv1/devices.json?created_at_gte={}&ignore_limit=true&is_forward_page=false&match=true&order=desc&order_by=id&page={}&paginated=true&per_page=200'.format(self.urls['devices'],myfilter,page+1)
        response = requests.get(allurl, headers=self.get_auth_header())
        if response.status_code == 200:
            json_data[page] = response.json()
#        return json_data
        while json_data[page]['total'] != json_data[page]['end_count_on_page']:
            page += 1
            allurl = '{}/apiv1/devices.json?ignore_limit=true&is_forward_page=false&match=true&order=desc&order_by=id&page={}&paginated=true&per_page=200'.format(self.urls['devices'],page+1)
            response = requests.get(allurl, headers=self.get_auth_header())
            if response.status_code == 200:
                json_data[page] = response.json()
        return json_data

    def get_groups(self):
        groupurl = '{}/imageservice/v1/oems/39a9391a/groups.json'.format(self.urls['images'])
        response = requests.get(groupurl, headers=self.get_auth_header())
        if response.status_code == 200:
            json_data = response.json()
            return json_data
        else:
            print('group: Status code: {}'.format(response.status_code))
            print(response.headers)
        return None
        json_data = response.json()
        return json_data

    def get_group_details(self, groupID=None):
        groupurl = '{}/imageservice/v1/group/{}'.format(self.urls['images'],groupID)
        response = requests.get(groupurl, headers=self.get_auth_header())
        if response.status_code == 200:
            json_data = response.json()
            return json_data['devices_dsns']
        else:
            print('groupID: {}, Status code: {}'.format(groupID,response.status_code))
            print(response.headers)
        return None

    def get_image_details(self, imageID=None):
        imageurl = '{}/imageservice/v1/host_images.json?[filter][id]={}'.format(self.urls['images'],imageID)
        response = requests.get(imageurl, headers=self.get_auth_header())
        if response.status_code == 200:
            json_data = response.json()
            return json_data[0]['image']
        else:
            print('imageID: {}, Status code: {}'.format(imageID,response.status_code))
            print(response.headers)
        return None

    def get_job_status(self, deviceName=None):
        jobsurl = '{}/imageservice/v1/{}/jobstatus.json'.format(self.urls['images'],deviceName)
        response = requests.get(jobsurl, headers=self.get_auth_header())
        if response.status_code == 200:
            json_data = response.json()
            return json_data
        elif response.status_code == 404:
            return None
        else:
            print('job_status, Status code: {}'.format(response.status_code))
            print(response.headers)
        return None

    def get_ota_jobs(self, statusstr=None, namestr=None):
        page = 1
        json_data = [None] * 100
        oem = '39a9391a'
        if namestr:
            jobsurl = '{}/imageservice/v1/job?{}&[filter][direction]=desc&[filter][job][oem]={}&[filter][job][status]={}&[filter][job][name]={}&[filter][order]=desc&[filter][order_by]=last_updated_at&[filter][sort_by]=last_updated_at&direction=desc&oem_id={}&order=desc&order_by=last_updated_at&page={}&paginated=true&per_page=200&sort_by=last_updated_at'.format(self.urls['images'],self.urls['env'],oem,statusstr,namestr,oem,page)
        else:
            jobsurl = '{}/imageservice/v1/job?{}&[filter][direction]=desc&[filter][job][oem]={}&[filter][order]=desc&[filter][order_by]=last_updated_at&[filter][sort_by]=last_updated_at&direction=desc&oem_id={}&order=desc&order_by=last_updated_at&page={}&paginated=true&per_page=200&sort_by=last_updated_at'.format(self.urls['images'],self.urls['env'],oem,oem,page)
        response = requests.get(jobsurl, headers=self.get_auth_header())
        if response.status_code == 200:
            json_data[page] = response.json()
        if json_data[page]['total'] == 0:
            return None
        while json_data[page]['total'] > json_data[page]['end_count_on_page']:
            page += 1
            jobsurl = '{}/imageservice/v1/job?{}&[filter][direction]=desc&[filter][job][oem]={}&[filter][job][status]={}&[filter][job][name]={}&[filter][order]=desc&[filter][order_by]=last_updated_at&[filter][sort_by]=last_updated_at&direction=desc&oem_id={}&order=desc&order_by=last_updated_at&page={}&paginated=true&per_page=200&sort_by=last_updated_at'.format(self.urls['images'],self.urls['env'],oem,statusstr,namestr,oem,page)
            response = requests.get(jobsurl, headers=self.get_auth_header())
            if response.status_code == 200:
                json_data[page] = response.json()
        return json_data

    def get_job_detail(self, jobID=None):
        jobsurl = '{}/imageservice/v1/job/{}.json'.format(self.urls['images'],jobID)
        response = requests.get(jobsurl, headers=self.get_auth_header())
        if response.status_code == 200:
            json_data = response.json()
            return json_data
        else:
            print('jobID: {}, Status code: {}'.format(jobID,response.status_code))
            print(response.headers)
        return None

    def get_job_devices(self, jobID=None):
        jobsurl = '{}/imageservice/v1/job/{}/devices.json'.format(self.urls['images'],jobID)
        response = requests.get(jobsurl, headers=self.get_auth_header())
        if response.status_code == 200:
            json_data = response.json()
            return json_data
        else:
            print('jobID: {}, Status code: {}'.format(jobID,response.status_code))
            print(response.headers)
        return None

    def get_dsn_details(self, deviceName=None):
        dsnurl = '{}/apiv1/dsns/{}.json'.format(self.urls['devices'],deviceName)
        response = requests.get(dsnurl, headers=self.get_auth_header())
        if response.status_code == 200:
            json_data = response.json()
            return json_data['device']
        else:
            if response.status_code == 429:
                logging.debug('429dsn_details... secondtry?')
                time.sleep(2)
                secondtry = self.get_dsn_details(deviceName)
                if secondtry:
                    return secondtry
                else:
                    print('deviceName: {}, Status code: {}'.format(deviceID,response.status_code))
                    print(response.headers)

        return None

    def get_user_details(self, userID=None):
        userurl = '{}/users/{}.json'.format(self.urls['users'],userID)
        response = requests.get(userurl, headers=self.get_auth_header())
        if response.status_code == 200:
            json_data = response.json()
            return json_data
        else:
            print('userID: {}, Status code: {}'.format(userID,response.status_code))
            print(response.headers)
        return None

    def get_last_connection(self, deviceID=None):
        connection_url = '{}/apiv1/devices/{}/connection_history/page/1/limit/1/field/event_time/order/desc.json'.format(self.urls['devices'],deviceID)
        response = requests.get(connection_url, headers=self.get_auth_header())
        if response.status_code == 200:
            json_data = response.json()
            try:
                if json_data[0]:
                    return json_data[0]['connection_history']
            except IndexError:
                return None
        else:
            if response.status_code == 429:
                logging.debug('429connection... secondtry?')
                time.sleep(2)
                secondtry = self.get_last_connection(deviceID)
                if secondtry:
                    return secondtry
                else:
                    print('ConnectiondeviceID: {}, Status code: {}'.format(deviceID,response.status_code))
                    print(response.headers)

        return None


    def get_properties(self, deviceID=None):
        properties_url = '{}/apiv1/devices/{}/properties.json'.format(self.urls['devices'],deviceID)
        response = requests.get(properties_url, headers=self.get_auth_header())
        if response.status_code == 200:
            json_data = response.json()
            return json_data
        else:
            if response.status_code == 429:
                logging.debug('429properties... secondtry?')
                time.sleep(2)
                secondtry = self.get_properties(deviceID)
                if secondtry:
                    return secondtry
                else:
                    print('propertiesdeviceID: {}, Status code: {}'.format(deviceID,response.status_code))
                    print(response.headers)

        return None

    def get_schedules(self, deviceID=None):
        schedules_url = '{}/apiv1/devices/{}/schedules.json'.format(self.urls['devices'],deviceID)
        response = requests.get(schedules_url, headers=self.get_auth_header())
        if response.status_code == 200:
            json_data = response.json()
            return json_data
        else:
            if response.status_code == 429:
                logging.debug('429schedules... secondtry?')
                time.sleep(2)
                secondtry = self.get_schedules(deviceID)
                if secondtry:
                    return secondtry
                else:
                    print('schedulesdeviceID: {}, Status code: {}'.format(deviceID,response.status_code))
                    print(response.headers)

        return None

    def start_ota_job(self, jobID):
        start_url = '{}/imageservice/v1/job/{}/start'.format(self.urls['images'],jobID)
        payload = {}
        data = requests.post(
            start_url,
            json=payload,
            headers=self.get_auth_header()
        )
        if data.status_code == 200:
            print('', end='')
            return True
        else:
            print('jobID: {}, Status code: {}'.format(jobID,data.status_code))
            print(data.headers)
            foo = input('funky state... need a human 1')
        return None
        
    def create_ota_job(self, groupID, imageID, name):
        payload = {'group_id': '{}'.format(groupID), 'image_id': '{}'.format(imageID), 'name': '{}'.format(name)}
        ota_job_url = '{}/imageservice/v1/job.json?group_id={}&image_id={}&name={}'.format(self.urls['images'],groupID, imageID, name)

        logging.debug('Generating OTA Job')
        data = requests.post(
            ota_job_url,
            json=payload,
            headers=self.get_auth_header()
        )
        if data.status_code == 201:
            json_data = data.json()
            try:
                jobs = self.start_ota_job(json_data['job']['id'])
                if jobs:
                    logging.debug('Starting OTA Job')
                    return json_data['job']['name']
            except KeyError as e:
                if json_data['id']:
                    logging.debug('2nd try')
                    jobs = self.start_ota_job(json_data['id'])
                    if jobs:
                        logging.debug('Starting OTA Job')
                        return json_data['name']
                pass
        elif data.status_code == 503 or data.status_code == 504:
            jobs = self.create_ota_job(groupID, imageID, name)
        else:
            print(data.status_code)
            print(data.headers)
            foo = input('funky state... need a human 3')
        exit()

    def delete_ota_job(self, jobID=None):

        logging.debug('Deleting job')
        delete_url = '{}/imageservice/v1/job/{}?{}'.format(self.urls['images'],jobID,self.urls['env'])

        # if the auth token doesnt exist or has expired, login to get a new one
        if (self.auth_token is None) or (self.expire_time <= time.time()):
            logging.debug('Auth Token expired, need to get a new one')
            self.login(self.username, self.password)

        self.auth_header = {'content-type': 'application/json',
                            'Accept': '*/*',
                            'Access-Control-Request-Method': 'DELETE',
                            'Access-Control-Request-Headers' : 'authorization,cache-control,content-type',
                            'Host': self.urls['host'],
                            'Origin' : self.urls['dashboard'],
                            'Authorization': 'auth_token ' + self.auth_token
                            }

        data = requests.options(
            delete_url,
            headers=self.auth_header
        )

        self.auth_header = {'content-type': 'application/json',
                            'Host': self.urls['host'],
                            'Origin' : self.urls['dashboard'],
                            'Authorization': 'auth_token ' + self.auth_token
                            }

        data = requests.delete(
            delete_url,
            headers=self.auth_header
        )
        if data.status_code == 200:
            return data.headers
        else:
            print(data.status_code)
            print(data.headers)
        return None

    def aylareboot(self, deviceID=None):

        reboot_url = '{}/apiv1/devices/{}/cmds/reboot.json?value=1'.format(self.urls['devices'],deviceID)
        payload = {}

        data = requests.put(
            reboot_url,
            json=payload,
            headers=self.get_auth_header()
        )
        if data.status_code == 200:
            return data
        else:
            print('Reboot - Status code: {}'.format(data.status_code))
            print(data.headers)

        return None

    def set_property(self, propertyID=None, value=None):

        seturl = '{}/apiv1/properties/{}/datapoints.json?{}'.format(self.urls['devices'],propertyID,self.urls['env'])
        payload = {"id":"{}".format(propertyID),"datapoint":{"value":"{}".format(value)}}

        logging.debug('SettingProperty')
        # if the auth token doesnt exist or has expired, login to get a new one
        if (self.auth_token is None) or (self.expire_time <= time.time()):
            logging.debug('Auth Token expired, need to get a new one')
            self.login(self.username, self.password)

        self.auth_header = {'content-type': 'application/json',
                            'Accept': '*/*',
                            'Access-Control-Request-Method': 'PUT',
                            'Access-Control-Request-Headers' : 'authorization,cache-control,content-type',
                            'Host': self.urls['host'],
                            'Origin' : self.urls['dashboard'],
                            'Authorization': 'auth_token ' + self.auth_token
                            }

        data = requests.options(
            seturl,
            headers=self.auth_header
        )

        data = requests.post(
            seturl,
            json=payload,
            headers=self.get_auth_header()
        )
        if data.status_code == 201:
            return data
        else:
            if data.status_code == 429:
                logging.debug('429set_properties... secondtry?')
                time.sleep(2)
                secondtry = self.set_property(propertyID, value)
                if secondtry:
                    return secondtry
                else:
                    print('propertyID: {}, Status code: {}'.format(propertyID,data.status_code))
                    print(data.headers)

        return None

    def deploy_image(self, dsn, imagePayload):
        
        deploy_url = '{}/apiv1/dsns/{}/deploy_image.json?{}'.format(self.urls['devices'],dsn,self.urls['env'])
        payload = {}
        
        logging.debug('Deploying image')
        # if the auth token doesnt exist or has expired, login to get a new one
        if (self.auth_token is None) or (self.expire_time <= time.time()):
            logging.debug('Auth Token expired, need to get a new one')
            self.login(self.username, self.password)

        self.auth_header = {'content-type': 'application/json',
                            'Accept': '*/*',
                            'Access-Control-Request-Method': 'PUT',
                            'Access-Control-Request-Headers' : 'authorization,cache-control,content-type',
                            'Host': self.urls['host'],
                            'Origin' : self.urls['dashboard'],
                            'Authorization': 'auth_token ' + self.auth_token
                            }

        data = requests.options(
            deploy_url,
            headers=self.auth_header
        )

        self.auth_header = {'content-type': 'application/json',
                            'Host': self.urls['host'],
                            'Origin' : self.urls['dashboard'],
                            'Authorization': 'auth_token ' + self.auth_token
                            }

        data = requests.put(
            deploy_url,
            json=imagePayload,
            headers=self.auth_header
        )
        if data.status_code == 200:
            return data.headers
        else:
            print(data.status_code)
            print(data.headers)
        return None

    def get_property_datapoints(self, propertyKey=None):

        data_point_url = '{}/apiv1/properties/{}/datapoints?per_page=100&is_forward_page=true&paginated=false&[filter][direction]=asc&[filter][order]=asc&[filter][order_by]=datapoint.created_at&[filter][sort_by]=datapoint.created_at&direction=asc&order=asc&order_by=datapoint.created_at'.format(self.urls['devices'],propertyKey)

        response = requests.get(data_point_url, headers=self.get_auth_header())
        data = response.json()

        return data

    def get_creds(self):
        creds_url = '{}/credentials'.format(self.urls['dashboard'])
        response = requests.get(creds_url, headers=self.get_auth_header())
        if response.status_code == 200:
            json_data = response.json()
        
            exit()
            return json_data
        else:
            print('Creds - Status code: {}'.format(response.status_code))
            print(response.headers)

        return None


    def set_app_active(self):

        if self.app_active_expire < time.time():

            if self.app_active_prop_id is None:
                prop = self.get_properties('GET_System_Error_Log', False)
                self.app_active_prop_id = prop['key']

            data_point_url = '{}/apiv1/properties/{}/datapoints.json'.format(self.urls['devices'],self.app_active_prop_id)

            self.app_active_expire = time.time() + self.app_active_ttl


    def login(self, email, password):
        """Logs in to the API service to get access token.

        Returns:
        A json value with access token.
        """
        self.headers = {'content-type': 'application/json',
                        'accept': 'application/json'}

        login_url = '{}/users/sign_in.json'.format(self.urls['login'])
        login_payload = {
            "user": {
                "email": email,
                "password": password,
                "application": {
                      "app_id": self.urls['app_id'],
                      "app_secret": self.urls['app_secret']
#                     "app_id": "Vac-iOS-dev-id",
#                     "app_secret ""Vac-iOS-dev-TFn4WRa14qNGae7eYh5M0HPSap0"
#                    "app_id": "SDA-JQ-id",
#                    "app_secret": "SDA-rQOiX-fGHZRq88Fzq4Yb3nxQFLk"
                }
            }
        }
        #print(login_payload)
        logging.debug('Generating token')
        data = requests.post(
            login_url,
            json=login_payload,
            headers=self.headers
        )
        # Example response:
        # {
        #    u'access_token': u'abcdefghijklmnopqrstuvwxyz123456',
        #    u'role': u'EndUser',
        #    u'expires_in': 86400,
        #    u'refresh_token': u'123456abcdefghijklmnopqrstuvwxyz',
        #    u'role_tags': []
        # }
     
        json_data = data.json()
        # update our auth token

        # try:
        self.auth_token = json_data['access_token']

        # except KeyError as e:
        #    print('{} unable to login for access_token.'.format(email))
        #    exit(1)
            
        # update our auth expiration time
        self.expire_time = time.time() + json_data['expires_in']

        logging.debug('Auth Token: %s expires at %s',
                      self.auth_token, self.expire_time)
        
        # return auth_token
        # return expire_time
