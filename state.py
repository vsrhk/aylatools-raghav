class State(object):
    """
    Current state definition
    """

    def __init__(self):
        print("Current state : ", str(self))

    def on_mode(self, mode):
        pass

    def __repr__(self):
        """
        Leverages the __str__ method to describe the State.
        """
        return self.__str__()

    def __str__(self):
        """
        Returns the name of the State.
        """
        return self.__class__.__name__ 
    