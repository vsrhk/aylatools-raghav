#!/usr/bin/env python3

from major.PyMajor import PyMajor
from major.PyAyla import PyAyla
import os,sys
import time, datetime
import argparse

debug = 0

def debugprint(instr):
    if debug == 1:
        print(instr)
    
def snippet(myin,myout):
    debugprint(myin)
    return myout+myin+'\n'

parser = argparse.ArgumentParser()
group = parser.add_mutually_exclusive_group(required=True)
group.add_argument('--MAC', default=None)
group.add_argument('--DSN', default=None)
parser.add_argument('--variable', default=None, required=True)
parser.add_argument('--value', default=0)
parser.add_argument('--email', default=argparse.SUPPRESS)
parser.add_argument('--password', default=argparse.SUPPRESS)
args = parser.parse_args()

aylainfo = PyAyla(args)
mac = args.MAC
dsn = args.DSN
propvariable = args.variable
propvalue = args.value

if (mac or dsn):

    pymajorClient = PyMajor(aylainfo.env, aylainfo.login_email, aylainfo.login_password)
    if debug == 1:
        if pymajorClient:
            print('Client is instantiated for setting a property')

    mydevice = {}
    details = {}
    allproperties = {}
    beforeprop = {}
    if mac:
       mydevice = pymajorClient.get_device_byMAC(mac)
       if mydevice['total'] == 0:
           print('Unknown MAC: \'{}\''.format(mac))
           exit(1)
       dsn = mydevice['devices'][0]['device']['dsn']
    
    if dsn:
       details =  pymajorClient.get_dsn_details(dsn)
       allproperties = pymajorClient.get_properties(details['id'])

    for count,each in enumerate(allproperties):
        beforeprop[details['id'],each['property']['name']] = each['property']

    propID = beforeprop[details['id'],propvariable]['key']

    # Initalize some stuff
    setprop = pymajorClient.set_property(propID, propvalue)
    if not setprop:
        foo = input("Problem with setting {} to {} on {}".format(propvariable,propvalue,details['id']))

    time.sleep(1)
    exit()

else:
    parser.print_help()
