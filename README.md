**Some scripts to monitor/search ayla (currently dev only)**

* ChargeCheck.py
    * Monitor a given group (--mongroup) for Offline *or* BatteryCharge=0 states

* CheckJobs.py
    * Check the status of all OTA jobs the user can see

* DeployGroup.py
    * Deploy (NOT OTA) an image to a group

* DevCheck.py (like ChargeCheck, but only email to me)

* GetData.py
    * Get all devices in env
    * If offline get the most recent connection history entry
    * Get all properties for each device
    * Get user info for each devices
    * Output to pickle files which ReturnVers read and outputs.

* OTAGroups.py
    * Create an OTA job to group(s) and start it.

* PropertyDatapoints.py
    * with MAC or DNS - specify a Property and get the datapoints for it.

* RebootAndVerify.py
    * Reboot and test 'goal' against GET_Robot_Firmware_Version after reboot.

* RebootGroup.py
    * Reboot a group

* ReturnVers.py
    * load mydevices, myproperties, myusers pickle files and output various things about the devices  

* SetVarGroup.py
    * Set a group with variable and value

* VolumeGroup.py
    * Set the volume on a group

* createKey.py
    * use to create encrypted keys for .aylaconfig file  

---
**Sample output of the structures returned from ayla API calls**

*More info can be found here:* [Ayla Networks Developer API](https://developer.aylanetworks.com/apibrowser/)

* device.sample
* properties.sample
* conection.sample
* jobstatus.sample
* image.sample
* job.sample

---
**Python class files**

* major/PyMajor.py

* major/PyKey.py

---
** How to setup for use **

python3 -m venv \`pwd`\
source bin/activate  
pip3 install -r requirements.txt

* create .aylaconfig file  

```
    ./createKey.py (enter password to be encyrpted)  
    
    edit .aylaconfig file in .ini style:
    [dev]  
        ayla_email = <your email/login>  
        ayla_password = <output from createKey.py>
        smtp_email = <your email>; defaults to ayla_email if not set
        smpt_password = <output from createKey.py>; defaults to ayla_password if not set

    [field]
        ayla_email = <your email/login>  
        ayla_password = <output from createKey.py>
        smtp_email = <your email>; defaults to ayla_email if not set
        smpt_password = <output from createKey.py>; defaults to ayla_password if not set
```
