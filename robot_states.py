from state import State

class Cleaning(State):
    """
    Cleaning State.    
    Can go to: 
        Pause (with error)
            Error#
            mode == 0
            SOM == (0, after)
        Pause (without error)
            mode == 0
            SOM == (0, before)
        Docking
            mode == 3
            ChSt == 0
            SOM == (3, after)
    Can not go to:
        Cleaning
    """
    def on_mode(self, mode):
        if mode == '3' and ChSt == 0:
            return Docking()
        elif mode == '0':
            return Paused()
        return self

class Docking(State):
    """
    Docking State.
    Can go to:
        Pause (with error)
            Error#
            mode == 0
            SOM == (0, after)
        Pause (without error)
            mode == 0
            SOM == (0, before)
        Docked
            mode == 3
            ChSt == 1
            SOM (3, after) around ??
        Cleaning
            mode == 2
            SOM == (2, before)
    
    Can not go to:
        Docking
    """
    def on_mode(self, mode):
        if mode == '3':
            return Docked()
        elif mode == '2': 
            return Cleaning()
        elif mode == '0':
            return Paused()
        return self

class Docked(State):
    """
    Docked state. 
    Can go to:
        Pause (with error)
            Error#
            mode == 0
            SOM == (0, after)
        Pause (without error)
            mode == 0
            SOM == (0, before)
        Cleaning
            mode == 2
            SOM == (2, before)
        
    Can not go to:
        -----
    """
    def on_mode(self, mode):
        if mode == '2': 
            return Cleaning()
        elif mode == '0':
            return Paused()
        return self