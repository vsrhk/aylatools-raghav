#!/usr/bin/env python3

import datetime
import PbApp_MAP_pb2
import argparse

def mapout(amap):
    keys = { 0: '.', 10: ':', 25: 'o', 50: '*', 75: '8', 100: '#' }
    bar = PbApp_MAP_pb2.PbMapData()
    bar.CopyFrom(amap)
    print('{} {}x{} WxH: {} {}sq'.format(bar.cellSize, bar.width, bar.height, (bar.width * bar.height), (bar.width * bar.height * bar.cellSize)))
    a = '{}'.format(bar.mapData)
    ba = bytearray(bar.mapData)
    bb = bytearray(len(ba)+1)
    for pos,value in enumerate(ba):
        bb[pos+1] = value
    bb[0] = 99
    for pos,value in enumerate(bb):
        if not (pos % bar.width):
            try:
                print(keys[value])
#                pass
            except:
                pass
        else:
            try:
                print(keys[value],end='')
#                pass
            except:
                pass


def updatemap(amap):
    bar = PbApp_MAP_pb2.PbMapData()
    bar.CopyFrom(amap)
    bar.height = amap.height * 20
    ba = bytearray(bar.mapData)
    bb = ba + ba + ba + ba
    bc = bb + bb + bb + bb + bb
    bar.mapData = bytes(bc)
    return(bar)

parser = argparse.ArgumentParser()
parser.add_argument('datafiles', metavar='propfile', type=str, nargs='+', help='files to read')
args = parser.parse_args()
datafiles = args.datafiles

readmap = ""
bar = PbApp_MAP_pb2.PbFloor()
foo = PbApp_MAP_pb2.PbFloor()
temp = PbApp_MAP_pb2.PbMapData()
newmap = PbApp_MAP_pb2.PbMapData()

for dfile in datafiles:
    with open(dfile, "rb") as f:
        dsn =  dfile.split('/',1)[0]
        readmap = f.read()
        print("{}:".format(dsn))
        bar = bar.FromString(readmap)
        print(bar.isDefault)
        foo.CopyFrom(bar)
        mapout(bar.map)
        temp.CopyFrom(bar.map)

# to dup/update an existing map x20 uncomment the next 4 lines
#        newmap = updatemap(temp)
#        mapout(newmap)
#        foo.map.MergeFrom(newmap)
#        foo.persistMap.MergeFrom(newmap)
        
# to write out the file... uncomment the next 5 lines
#for dfile in datafiles:
#    dfile = dfile+'.mod'
#    with open(dfile, "wb") as f:
#          newbytes = foo.SerializeToString(foo)
#          f.write(newbytes)

exit(0)

#        print("{}".format(length(bar.FromString(foo).map.mapData)))
#        print("{}".format(mymap(bar.FromString(amap).map)))
#        print("\t{}: {}".format("id",bar.FromString(amap).id))
#        print("\t{}: {}".format("name",bar.FromString(amap).name))
#        print("\t{}: {}".format("hashid",bar.FromString(amap).hashID))
#        print("\t{}: {}".format("type",bar.FromString(amap).type))

