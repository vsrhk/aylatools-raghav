#!/usr/bin/env python3

import datetime
import PbApp_SCM_pb2
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('datafiles', metavar='propfile', type=str, nargs='+', help='files to read')
args = parser.parse_args()
datafiles = args.datafiles

foo = ""

for dfile in datafiles:
    with open(dfile, "rb") as f:
        dsn =  dfile.split('/',1)[0]
        foo = f.read()
        bar = PbApp_SCM_pb2.SCMCleanStats()
        print("{},".format(dsn),end='')
        
#        print("\t{}: {}".format("startTime",datetime.datetime.fromtimestamp(bar.FromString(foo).startTime)))
#        print("\t{}: {}".format("cleanTime",bar.FromString(foo).cleanTime))
#        print("\t{}: {}".format("cleanArea",bar.FromString(foo).cleanArea))
#        print("\t{}: {}".format("totalCleanArea",bar.FromString(foo).totalCleanArea))
#        print("\t{}: {}".format("TotalCleanTime",bar.FromString(foo).TotalCleanTime))
#        print("\t{}: {}".format("isCleanFinished",bar.FromString(foo).isCleanFinished))
#        print("\t{}: {}".format("totalDustBinChangeCount",bar.FromString(foo).totalDustBinChangeCount))
#        print("\t{}: {}".format("totalCleanCount",bar.FromString(foo).totalCleanCount))
        print("{},".format(datetime.datetime.fromtimestamp(bar.FromString(foo).startTime)),end='')
        print("{},".format(bar.FromString(foo).cleanTime),end='')
        print("{},".format(bar.FromString(foo).cleanArea),end='')
        print("{},".format(bar.FromString(foo).totalCleanArea),end='')
        print("{},".format(bar.FromString(foo).TotalCleanTime),end='')
        print("{},".format(bar.FromString(foo).isCleanFinished),end='')
        print("{},".format(bar.FromString(foo).totalDustBinChangeCount),end='')
        print("{}".format(bar.FromString(foo).totalCleanCount))

