#!/usr/bin/env python3

from major.PyMajor import PyMajor
from major.PyAyla import PyAyla
import pickle
import os,sys
import time, datetime
import argparse
from myemail import send_myemail
from properties import debugprint, snippet, getproperties

parser = argparse.ArgumentParser()
parser.add_argument('--mongroup', required=True)
parser.add_argument('--constatus', default='Offline')
parser.add_argument('--email', default=argparse.SUPPRESS)
parser.add_argument('--password', default=argparse.SUPPRESS)
args = parser.parse_args()

aylainfo = PyAyla(args)
mongroup = args.mongroup
constatus = args.constatus

pymajorClient = PyMajor(aylainfo.env, aylainfo.login_email, aylainfo.login_password)
if pymajorClient:
    print('Client is instantiated for {}'.format(mongroup))

# Init some email settings
receiver_emails = {
    'jschectman@sharkninja.com': 'JohnS',
}

message = ''
device_msg = ''

# Get the list of visible groups from ayla for logged in user
mygroups = {}
visiblegroups = pymajorClient.get_groups()

# setup mygroups dictonary with { count : group }
for count, group in enumerate(visiblegroups):
    mygroups[count] = group

thedevices = []
for key, value in mygroups.items():
    debugprint('{} = {}'.format(value['group']['id'],value['group']['name']))
    if value['group']['name'] == mongroup:
        debugprint('getting device info for {}: {}'.format(value['group']['id'],value['group']['name']))
        message = snippet('{}: {}'.format(value['group']['name'],value['group']['id']),message)
        thedevices = (pymajorClient.get_group_details(value['group']['id']))

if len(thedevices) != 0:
    debugprint(thedevices)
    message = snippet('Number of devices in current group: {}'.format(len(thedevices)),message)

# setup mydevices dictonary with { count : device }
mydevices = {}
for count, device in enumerate(thedevices):
    details = pymajorClient.get_dsn_details(device)
    if details:
        mydevices[count] = details

# Initalize some stuff
allproperties = {}
myproperties = {}
status = 1
for key, value in mydevices.items():
    allproperties = pymajorClient.get_properties(value['id'])
    if not allproperties:
        # sleep 5 and try again, before failing
        time.sleep(5)
        allproperties = pymajorClient.get_properties(value['id'])

    debugprint(allproperties)
    if value['connection_status'] == constatus:
        status = 0

    # for each device save the all properties indexed by ['id']['<property name>']
    for count,each in enumerate(allproperties):
        myproperties[value['id'],each['property']['name']] = each['property']

# search thru all the devices and record one with GET_Charging_Status = 0
device_msg,badcount = getproperties(pymajorClient, mydevices, myproperties, constatus)

sender_email = aylainfo.smtp_email
now = datetime.datetime.now()
subject = now.strftime("%Y-%m-%d %H:%M")+" dev {} Rack Status".format(mongroup)

if status == 0:
    # if we've already saved the msg file... compare it to current output
    # if it doesn't exist... save it and send an email
    try:
        os.remove("{}_dev_good".format(mongroup))
    except OSError:
        pass

    if os.path.isfile('{}_dev_device_msg'.format(mongroup)):
        with open('{}_dev_device_msg'.format(mongroup), 'rb') as f:
            old_device_msg = pickle.load(f)
        if device_msg != old_device_msg:
            print('Message different...')
        else:
            print('Message identical...')
            exit()

    print('Dumping dev_device_msg...')
    with open('{}_dev_device_msg'.format(mongroup), 'wb') as f:
        pickle.dump(device_msg, f, 0)

    print('Trying to send email....')
    message = snippet('Devices with \'issues\': {}'.format(badcount),message)
    message = message+device_msg
    print(message)
    foo = input('Send?')

else:
    if os.path.isfile('{}_dev_good'.format(mongroup)):
        print('All Devices [still] Good!')
        exit()
    else:
        message = snippet('\nDevices are good again!',message)
        print('All Devices Good again!')
        f = open("{}_dev_good".format(mongroup),"w+")
        try:
            os.remove("{}_dev_device_msg".format(mongroup))
        except OSError:
            pass

send_myemail(sender_email, aylainfo.smtp_password, receiver_emails, subject, message)
exit()
