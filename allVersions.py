#!/usr/bin/env python3

from major.PyMajor import PyMajor
from major.PyAyla import PyAyla
import os, sys
import time
import datetime
import argparse

debug = 0

def debugprint(instr):
    if debug == 1:
        print(instr)
    
def snippet(myin,myout):
    debugprint(myin)
    return myout+myin+'\n'
    
parser = argparse.ArgumentParser()
parser.add_argument('--email', default=argparse.SUPPRESS)
parser.add_argument('--password', default=argparse.SUPPRESS)
args = parser.parse_args()

aylainfo = PyAyla(args)

# Initalize some stuff
xdevices = {}
mydevices = {}
myproperties = {}

pymajorClient = PyMajor(aylainfo.env, aylainfo.login_email, aylainfo.login_password)
if debug == 1:
    if pymajorClient:
        print('Client is instantiated for DSN info')

status = 0
oldest = "2019-09-30+00:00:00"

alldevices1 = pymajorClient.get_device_byActivation(oldest,'RV1000')
if alldevices1:
    print('Total RV1000 devices: {}'.format(alldevices1[0]['total']))

alldevices2 = pymajorClient.get_device_byActivation(oldest,'RV1000A')
if alldevices2:
    print('Total RV1000A devices: {}'.format(alldevices2[0]['total']))

start = 0
if alldevices1:
    for each in alldevices1:
        if each:
            for count, details in enumerate(each['devices'], start):
                debugprint('{} {}'.format(count,details))
                mydevices[count] = details['device']
            start = each['end_count_on_page']

start2 = start
start = 0
if alldevices2:
    for each in alldevices2:
        if each:
            for count, details in enumerate(each['devices'], start):
                debugprint('{} {}'.format(count+start2,details))
                mydevices[count+start2] = details['device']
            start = each['end_count_on_page']

# loop thru all the devices and grab the 'right' ones
for key, value in mydevices.items():
    try:
        if value['oem_model'] == "RV1000" or value['oem_model'] == "RV1000A":
            try:
                allproperties = pymajorClient.get_properties(value['key'])
                if allproperties:
                # for each device save the all properties indexed by ['id']['<property name>']
                    for count,each in enumerate(allproperties):
                        myproperties[value['key'],each['property']['name']] = each['property']
                try:
                    FW_VERSION = myproperties[value['key'],'OTA_FW_VERSION']['value']
                    if not FW_VERSION:
                        FW_VERSION = myproperties[value['key'],'GET_Nav_Module_App_Version']['value']
                except:
                    print('{},{},{},{}'.format('Unavailable',value['dsn'],value['mac'],value['oem_model']))
                    pass
                print('{},{},{},{}'.format(FW_VERSION,value['dsn'],value['mac'],value['oem_model']))

            except:
                print('{},{},{}'.format(value['dsn'],value['mac'],'except Unavailable'))
                foo = input('la la la')
                pass
        else:
            print('',end='')
    except:
        print('some error')
        pass

exit()
