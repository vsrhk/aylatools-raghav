for foo in $(seq 1 502); do
    echo '----------' ${foo} '----------'
    python3 OTAGroups.py --imageid 14279 8334 8335 8348 8349 8350 8351 8352 8353 8354 8355 8356 8358 --goal S1.0.64
    if [ $? == 0 ]; then
	python3 OTAGroups.py --imageid 14278 8333 8336 8357 --goal S1.0.64
	if [ -e "PAUSE" ]; then
	    read -p "Pausing...." pauseme
	fi
	if [ $? == 0 ]; then
	    python3 OTAGroups.py --imageid 14297 8334 8335 8348 8349 8350 8351 8352 8353 8354 8355 8356 8358 --goal S1.0.60
	    if [ $? == 0 ]; then
		python3 OTAGroups.py --imageid 14298 8333 8336 8357 --goal S1.0.60
		if [ $? == 0 ]; then
		    echo 'Round' ${foo} 'Complete'
		fi
	    fi
	fi
    fi
    python3 ChargeCheck.py --mongroup MonitorGroup01
done
