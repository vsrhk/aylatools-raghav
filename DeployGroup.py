#!/usr/bin/env python3

from major.PyMajor import PyMajor
from major.PyAyla import PyAyla
import os,sys
import time
import argparse

debug = 0

def debugprint(instr):
    if debug == 1:
        print(instr)
    
def snippet(myin,myout):
    debugprint(myin)
    return myout+myin+'\n'

parser = argparse.ArgumentParser()
parser.add_argument('groups', metavar='N', type=int, nargs='+', help='groups to OTA')
parser.add_argument('--imageid', required=True)
parser.add_argument('--goal', required=True)
parser.add_argument('--wait', type=int, default=60)
parser.add_argument('--email', default=argparse.SUPPRESS)
parser.add_argument('--password', default=argparse.SUPPRESS)
args = parser.parse_args()

aylainfo = PyAyla(args)
groups = args.groups
imageID = args.imageid
goal = args.goal
waittime = args.wait

pymajorClient = PyMajor(aylainfo.env, aylainfo.login_email, aylainfo.login_password)
if pymajorClient:
    print('Client is instantiated for deploy')

message = ''
beforedev = {}

totaldevices = 0
for groupID in groups:
    thedevices = pymajorClient.get_group_details(groupID)
    if thedevices:
        totaldevices = totaldevices + len(thedevices)
        debugprint(thedevices)
        message = snippet('Number of devices in current group: {}'.format(len(thedevices)),message)
    else:
        print('No devices in this group: {}'.format(groupID))
        exit(1)

    # setup beforedev dictonary with { count : device }
    for count, device in enumerate(thedevices, start=totaldevices-len(thedevices)):
        details = pymajorClient.get_dsn_details(device)
        if details:
            beforedev[count] = details

print('Total devices: {}'.format(totaldevices))

theimage = pymajorClient.get_image_details(imageID)
if not theimage:
    print('No image of this ID: {}'.format(imageID))
    exit()

# Initalize some stuff
allproperties = {}
beforeprop = {}
for key, value in beforedev.items():
    allproperties = pymajorClient.get_properties(value['id'])
    if not allproperties:
        # sleep 5 and try again, before failing
        time.sleep(5)
        allproperties = pymajorClient.get_properties(value['id'])

    debugprint(allproperties)

    # for each device save the all properties indexed by ['id']['<property name>']
    for count,each in enumerate(allproperties):
        beforeprop[value['id'],each['property']['name']] = each['property']

# deploy to thedevices - sleep 'waittime' between iterations of loop - default is 60
for count, device in enumerate(thedevices):
    print("Device: {}".format(device))
    payload = {"dsn":"{}".format(device),
               "image":
               {"ota_type":"{}".format(theimage["ota_type"]),
                "ota_size":int(theimage["ota_size"]),
                "deploy_url":"{}".format(theimage["deploy_url"]),
                "sw_version":"{}".format(theimage["sw_version"]),
                "source":"{}".format(theimage["source"]),
                "checksum":"{}".format(theimage["checksum"]),
                "api_url":None
               }
    }
    mydeploy = pymajorClient.deploy_image(device,payload)
    if mydeploy:
        print(mydeploy['Status'],mydeploy['Text'])

    print("sleeping {}".format(waittime))
    time.sleep(waittime)

# setup afterdev dictonary with { count : device }
afterdev = {}
for count, device in enumerate(thedevices):
    details = pymajorClient.get_dsn_details(device)
    if details:
        afterdev[count] = details

# Initalize some stuff
allproperties = {}
afterprop = {}
for key, value in afterdev.items():
    allproperties = pymajorClient.get_properties(value['id'])
    if not allproperties:
        # sleep 5 and try again, before failing
        time.sleep(5)
        allproperties = pymajorClient.get_properties(value['id'])

    debugprint(allproperties)

    # for each device save the all properties indexed by ['id']['<property name>']
    for count,each in enumerate(allproperties):
        afterprop[value['id'],each['property']['name']] = each['property']

    # loop on properties... checking Robot version
    test = afterprop[value['id'],'GET_Robot_Firmware_Version']['value']
    wait = 1
    while not test.endswith(goal):
        if wait == 20:
            foo = input('Something Stuck!... need a human to reset wait?')
            wait = 0
        wait += 1
        time.sleep(10)
        allproperties = pymajorClient.get_properties(value['id'])
        if allproperties:
            for count,each in enumerate(allproperties):
                afterprop[value['id'],each['property']['name']] = each['property']
        test = afterprop[value['id'],'GET_Robot_Firmware_Version']['value']

    print('{} is good'.format(value['id']))

exit()
