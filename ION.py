#!/usr/bin/env python3

from major.PyMajor import PyMajor
from major.PyAyla import PyAyla
import os, sys
import time
import datetime
import pickle
import pgeocode
import argparse
from myemail import send_myemail

debug = 0

def debugprint(instr):
    if debug == 1:
        print(instr)
    
def snippet(myin,myout):
    debugprint(myin)
    return myout+myin+'\n'
    
def set_prop(who, propid, prop, val):
    try:
        setprop = pymajorClient.set_property(propid, val)
        if not setprop:
            print("Problem with {} on {}".format(prop,who))
            return False
    except KeyError as e:
        print("Problem with {} on {}".format(prop,who))
        pass
        return False
    return True

def deploy_toProd(mac, dsn, animage):
#    return True
    payload = {"dsn":"{}".format(dsn),
               "image":
               {"ota_type":"{}".format(animage["ota_type"]),
                "ota_size":int(animage["ota_size"]),
                "deploy_url":"{}".format(animage["deploy_url"]),
                "sw_version":"{}".format(animage["sw_version"]),
                "source":"{}".format(animage["source"]),
                "checksum":"{}".format(animage["checksum"]),
                "api_url":None
               }
    }
    mydeploy = pymajorClient.deploy_image(dsn,payload)
    if mydeploy:
        if os.path.isfile('myslack.py'):
            os.system("./myslack.py @here ____ deployed image to {}".format(value['mac']))
        time.sleep(waittime)
        return True

    return False

def mylocale(zipcode):
    tryme = ['US','CO','CA','GB']
# these are supported... but let's wait until we see one before we add them    
# Andorra (AD), Argentina (AR), American Samoa (AS), Austria (AT), Australia (AU), Åland Islands (AX), Bangladesh (BD), Belgium (BE), Bulgaria (BG), Bermuda (BM), Brazil (BR), Belarus (BY), Canada (CA), Switzerland (CH), Colombia (CO), Costa Rica (CR), Czechia (CZ), Germany (DE), Denmark (DK), Dominican Republic (DO), Algeria (DZ), Spain (ES), Finland (FI), Faroe Islands (FO), France (FR), United Kingdom of Great Britain and Northern Ireland (GB), French Guiana (GF), Guernsey (GG), Greenland (GL), Guadeloupe (GP), Guatemala (GT), Guam (GU), Croatia (HR), Hungary (HU), Ireland (IE), Isle of Man (IM), India (IN), Iceland (IS), Italy (IT), Jersey (JE), Japan (JP), Liechtenstein (LI), Sri Lanka (LK), Lithuania (LT), Luxembourg (LU), Latvia (LV), Monaco (MC), Republic of Moldova (MD), Marshall Islands (MH), The former Yugoslav Republic of Macedonia (MK), Northern Mariana Islands (MP), Martinique (MQ), Malta (MT), Mexico (MX), Malaysia (MY), New Caledonia (NC), Netherlands (NL), Norway (NO), New Zealand (NZ), Philippines (PH), Pakistan (PK), Poland (PL), Saint Pierre and Miquelon (PM), Puerto Rico (PR), Portugal (PT), Réunion (RE), Romania (RO), Russian Federation (RU), Sweden (SE), Slovenia (SI), Svalbard and Jan Mayen Islands (SJ), Slovakia (SK), San Marino (SM), Thailand (TH), Turkey (TR), Ukraine (UA), United States of America (US), Uruguay (UY), Holy See (VA), United States Virgin Islands (VI), Wallis and Futuna Islands (WF), Mayotte (YT), South Africa (ZA)

    for country in tryme:
        # Init geo info for country
        myloc = pgeocode.Nominatim(country)
        qlocale = myloc.query_postal_code(zipcode)
        if type(qlocale.place_name) == float:
            continue
        else:
            return qlocale

parser = argparse.ArgumentParser()
parser.add_argument('--oldest', default="2019-09-06+10:00:00")
parser.add_argument('--imageV', default=None)
parser.add_argument('--imageM', default=None)
parser.add_argument('--wait', type=int, default=60)
parser.add_argument('--email', default=argparse.SUPPRESS)
parser.add_argument('--password', default=argparse.SUPPRESS)
args = parser.parse_args()

aylainfo = PyAyla(args)
oldest = args.oldest
imageV = args.imageV
imageM = args.imageM
waittime = args.wait

# Initalize some stuff
mydevices = {}
myproperties = {}
eachprop = {}
theimageV = {}
theimageM = {}

# Init some email settings
receiver_emails = {
    'wrazzaboni@sharkninja.com': 'Warren',
    'spirdy@sharkninja.com': 'Pirdy',
    'jschectman@sharkninja.com': 'JohnS',
}
message = ''

    
pymajorClient = PyMajor(aylainfo.env, aylainfo.login_email, aylainfo.login_password)
if debug == 1:
    if pymajorClient:
        print('Client is instantiated for ION devices')

status = 0
offline = 0
alldevices1 = pymajorClient.get_device_byActivation(oldest,'RV2019QF')
if alldevices1:
    if debug == 1:
        print('Total devices: {}'.format(alldevices1[0]['total']))

alldevices2 = pymajorClient.get_device_byActivation(oldest,'RV2019QFA')
if alldevices2:
    if debug == 1:
        print('Total devices: {}'.format(alldevices2[0]['total']))

if alldevices1 or alldevices2:
    if debug == 1:
        if alldevices1 and alldevices2:
            print('Total: {}'.format(alldevices1[0]['total']+alldevices2[0]['total']))
            exit()
        if alldevices1:
            print('Total: {}'.format(alldevices1[0]['total']))
            exit()
        if alldevices2:
            print('Total: {}'.format(alldevices2[0]['total']))
            exit()
else:
    utcnow = datetime.datetime.utcnow()
    with open('ION_oldest', 'w') as g:
        g.write(utcnow.strftime("%Y-%m-%d+%H:%M:%S"))
        exit()

with open('ION_mac', 'w') as g:
    g.write('')

if imageV:
    theimageV = pymajorClient.get_image_details(imageV)
    if not theimageV:
        print('No image of this ID: {}'.format(imageV))
        exit()

if imageM:
    theimageM = pymajorClient.get_image_details(imageV)
    if not theimageM:
        print('No image of this ID: {}'.format(imageV))
        exit()

start = 0
if alldevices1:
    for each in alldevices1:
        if each:
            for count, details in enumerate(each['devices'], start):
                debugprint('{} {}'.format(count,details))
                mydevices[count] = details['device']
            start = each['end_count_on_page']

start2 = start
start = 0
if alldevices2:
    for each in alldevices2:
        if each:
            for count, details in enumerate(each['devices'], start):
                debugprint('{} {}'.format(count+start2,details))
                mydevices[count+start2] = details['device']
            start = each['end_count_on_page']

# loop thru all the devices and grab the 'right' ones
for key, value in mydevices.items():
    try:
        if value['product_name'] == "Shark ION Robot":
            if os.path.isfile('myslack.py'):
                os.system("./myslack.py ________ {}".format(value['mac']))
#        if value['product_name'] == "DevShark ION Robot":
            try:
                if value['oem_model'] == "RV2019QF" or value['oem_model'] == "RV2019QFA":
                    locale = value['locality']
                    if locale is None :
                        if locale is None:
                            if os.path.isfile('myslack.py'):
                                os.system("./myslack.py @here skipping {} for no zipcode".format(value['mac']))
                            continue

                    qlocale = mylocale(locale)
                    if os.path.isfile('myslack.py'):
                        os.system("./myslack.py ________ {}".format(qlocale.state_name))
                    if qlocale.state_name != "Massachusetts" and locale != "111311" and locale != "37115":
                        details =  pymajorClient.get_dsn_details(value['dsn'])
                        if details['user_id']:
#                        if not details['user_id']:
                            if os.path.isfile('myslack.py'):
                                os.system("./myslack.py @here {} has a user_id".format(value['mac']))
                            continue
                        status = 1
                        if value['connection_status'] != "foo":
                            if value['connection_status'] == "Offline":
                                if os.path.isfile('myslack.py'):
                                    os.system("./myslack.py @here {} is offline".format(value['mac']))
                            activated = details['activated_at']
                            message = snippet('----------', message)
                            message = snippet('OEM Model: {}'.format(value['oem_model']), message)
                            message = snippet('MAC: {}'.format(value['mac']), message)
                            message = snippet('Activated: {}'.format(activated), message)
                            message = snippet('Locality: {} {} {}'.format(qlocale.place_name, qlocale.state_name, qlocale.postal_code), message)
                            myproperties = pymajorClient.get_properties(details['id'])
                            for count,each in enumerate(myproperties):
                                eachprop[each['property']['name']] = each['property']
                            message = snippet('Version: {}'.format(eachprop['GET_Nav_Module_App_Version']['value']), message)
                            if eachprop['GET_Charging_Status']['value'] == 0:
                                if os.path.isfile('myslack.py'):
                                    os.system("./myslack.py @here ____ {} is not charging".format(value['mac']))
                            else:
                                if value['connection_status'] == "Online":
                                    if os.path.isfile('myslack.py'):
                                        os.system("./myslack.py @here {} is online".format(value['mac']))
                            # reset Volume
                            soundID = eachprop['SET_Robot_Volume_Control']['key']
                            set_prop(details['id'],soundID,'SET_Robot_Volume_Control',25)
                            # reset FactoryDefaults
                            factoryID = eachprop['SET_Reset_Factory_Defaults']['key']
#                            set_prop(details['id'],factoryID,'SET_Reset_Factory_Defaults',1)
                            # deploy a PROD image
                            if os.path.isfile('myslack.py'):
                                os.system("./myslack.py @here version: {}".format(eachprop['GET_Nav_Module_App_Version']['value']))
                            if imageM and value['oem_model'] == "RV2019QFA":
                                if deploy_toProd(value['mac'],value['dsn'],theimageM):
                                    with open('ION_mac', 'a') as g:
                                        # output MAC so we can reset WiFi
                                        g.write('{} '.format(value['mac']))
                            if imageV and value['oem_model'] == "RV2019QF":
                                if deploy_toProd(value['mac'],value['dsn'],theimageV):
                                    with open('ION_mac', 'a') as g:
                                        # output MAC so we can reset WiFi
                                        g.write('{} '.format(value['mac']))

                    else:
                        msg = ""
                        if qlocale.state_name == "Massachusetts":
                            msg = "State is Mass {}".format(locale)
                        if locale == "111311":
                            msg = "Zip is 111311"
                        if locale == "37115":
                            msg = "Zip is 37115"
                        if os.path.isfile('myslack.py'):
                            os.system("./myslack.py @here ____ {}: {}".format(value['mac'],msg))

            except KeyError as e:
                print(e)
                pass
    except KeyError as e:
        print(e)
        pass

sender_email = aylainfo.smtp_email
utcnow = datetime.datetime.utcnow()
now = datetime.datetime.now()

subject = now.strftime("%Y-%m-%d %H:%M")+" Shark ION Robot devices in Dev"

if status == 1:
    # if we've already saved the msg file... compare it to current output
    # if it doesn't exist... save it and send an email

    if os.path.isfile('ION_device_msg'):
        with open('ION_device_msg', 'rb') as f:
            old_device_msg = pickle.load(f)
        if message != old_device_msg:
            print('Message different...')
        else:
            if offline == 0:
                with open('ION_oldest', 'w') as g:
                    g.write(utcnow.strftime("%Y-%m-%d+%H:%M:%S"))
                    exit()

    print('Dumping ION_device_msg...')
    with open('ION_device_msg', 'wb') as f:
        pickle.dump(message, f, 0)

    print('Trying to send email....')
    print(message)

with open('ION_oldest', 'w') as g:
    g.write(utcnow.strftime("%Y-%m-%d+%H:%M:%S"))

#exit()
if len(message) > 0:
    send_myemail(sender_email, aylainfo.smtp_password, receiver_emails, subject, message)
exit()
