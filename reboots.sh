for foo in $(seq 1 420); do
    echo '----------' ${foo} '----------'
    python3 RebootGroup.py 8225
    if [ $? == 0 ]; then
	echo 'Round' ${foo} 'Complete'
    fi
    if [ -e "PAUSE" ]; then
       read -p "Pausing...." pauseme
    fi
done
