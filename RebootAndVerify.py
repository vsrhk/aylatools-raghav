#!/usr/bin/env python3

from major.PyMajor import PyMajor
from major.PyAyla import PyAyla
import os,sys
import time, datetime
import argparse

debug = 0

def debugprint(instr):
    if debug == 1:
        print(instr)
    
def snippet(myin,myout):
    debugprint(myin)
    return myout+myin+'\n'

parser = argparse.ArgumentParser()
parser.add_argument('groups', metavar='N', type=int, nargs='+', help='groups to OTA')
parser.add_argument('--goal', required=True)
parser.add_argument('--wait', type=int, default=60)
parser.add_argument('--email', default=argparse.SUPPRESS)
parser.add_argument('--password', default=argparse.SUPPRESS)
args = parser.parse_args()

aylainfo = PyAyla(args)
groups = args.groups
goal = args.goal
waittime = args.wait

pymajorClient = PyMajor(aylainfo.env, aylainfo.login_email, aylainfo.login_password)
if pymajorClient:
    print('Client is instantiated for reboot')

message = ''
beforedev = {}

totaldevices = 0
for groupID in groups:
    thedevices = pymajorClient.get_group_details(groupID)
    if len(thedevices) != 0:
        totaldevices = totaldevices + len(thedevices)
        debugprint(thedevices)
        message = snippet('Number of devices in current group: {}'.format(len(thedevices)),message)
    else:
        print('No devices in this group: {}'.format(groupID))
        exit(1)

    # setup beforedev dictonary with { count : device }
    for count, device in enumerate(thedevices, start=totaldevices-len(thedevices)):
        details = pymajorClient.get_dsn_details(device)
        if details:
            beforedev[count] = details

print('Total devices: {}'.format(totaldevices))

# Initalize some stuff
allproperties = {}
beforeprop = {}
for key, value in beforedev.items():
    allproperties = pymajorClient.get_properties(value['id'])
    if not allproperties:
        # sleep 5 and try again, before failing
        time.sleep(5)
        allproperties = pymajorClient.get_properties(value['id'])

    debugprint(allproperties)

    # for each device save the all properties indexed by ['id']['<property name>']
    for count,each in enumerate(allproperties):
        beforeprop[value['id'],each['property']['name']] = each['property']

# deploy to thedevices - sleep waittime before exiting
for key, value in beforedev.items():
    PCBVer = beforeprop[value['id'],'GET_Main_PCB_FW_Version']['value']
    if value['connection_status'] == "Online" and PCBVer != "SharkM0.0.0":
        try:
            propID = beforeprop[value['id'],'SET_Robot_Volume_Control']['key']
            setprop = pymajorClient.set_property(propID, 25)
            if not setprop:
                print("Volume Problem with {}".format(value['id']))
                pass
        except KeyError as e:
            pass
        try:
            propID = beforeprop[value['id'],'SET_Reset_Factory_Defaults']['key']
            setprop = pymajorClient.set_property(propID, 1)
            if not setprop:
                foo = input("Reset Problem with {}".format(value['id']))
        except KeyError as e:
            foo = input("Problem finding Reset with {}".format(value['id']))
            pass
                    
    else:
        foo = input("{} is offline or PCBVer is {}".format(value['id'],PCBVer))

    time.sleep(5)
print('sleep after reboots...')
time.sleep(waittime)

# setup afterdev dictonary with { count : device }
afterdev = {}
for groupID in groups:
    thedevices = pymajorClient.get_group_details(groupID)
    if len(thedevices) != 0:
        totaldevices = totaldevices + len(thedevices)
        debugprint(thedevices)
        message = snippet('Number of devices in current group: {}'.format(len(thedevices)),message)
    else:
        print('No devices in this group: {}'.format(groupID))
        exit(1)

    # setup afterdev dictonary with { count : device }
    for count, device in enumerate(thedevices, start=totaldevices-len(thedevices)):
        details = pymajorClient.get_dsn_details(device)
        if details:
            afterdev[count] = details

# Initalize some stuff
allproperties = {}
afterprop = {}
for key, value in afterdev.items():
    allproperties = pymajorClient.get_properties(value['id'])
    if not allproperties:
        # sleep 5 and try again, before failing
        time.sleep(5)
        allproperties = pymajorClient.get_properties(value['id'])

    debugprint(allproperties)

    # for each device save the all properties indexed by ['id']['<property name>']
    for count,each in enumerate(allproperties):
        afterprop[value['id'],each['property']['name']] = each['property']

for key, value in afterdev.items():
    # loop on properties... checking Robot version
    test = afterprop[value['id'],'GET_Robot_Firmware_Version']['value']
    print(test)
    wait = 1
    while not test.endswith(goal):
        if wait == 60:
            foo = input('Something Stuck!... need a human to reset wait?')
            wait = 0
        wait += 1
        time.sleep(15)
        allproperties = pymajorClient.get_properties(value['id'])
        if allproperties:
            for count,each in enumerate(allproperties):
                afterprop[value['id'],each['property']['name']] = each['property']
        test = afterprop[value['id'],'GET_Robot_Firmware_Version']['value']
        
    print('{} is good'.format(value['id']))
            
exit()

