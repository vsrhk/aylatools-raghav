#!/usr/bin/env python3

import sys
import time
import datetime
import pickle
from pprint import pprint

debug = 0

def debugprint(instr):
    if debug == 1:
        print(instr)
    
def snippet(myin,myout):
    debugprint(myin)
    return myout+myin+'\n'
    
if len(sys.argv) != 1:
    print('usage:  python3 ReturnVers.py\n')
else:

    # Initalize some stuff
    mydevices = {}
    myproperties = {}

    with open('mydevices.pickle', 'rb') as f:
        mydevices = pickle.load(f)

    with open('myproperties.pickle', 'rb') as f:
        myproperties = pickle.load(f)

    with open('myusers.pickle', 'rb') as f:
        myusers = pickle.load(f)

    # loop thru all the devices and extract OTA_FW_VERSION
    for key, value in mydevices.items():
        try:
            print(value['dsn'],end='')
        except KeyError as e:
            print('Not available')
            pass

        try:
            print(',{}'.format(value['mac']),end='')
        except KeyError as e:
            print(',{}'.format('Not available'))
            pass

        try:
            print(',{}'.format(myusers[value['user_id']]['email']))
        except KeyError as e:
            print(',{}'.format('Not available'))

    exit()
