#!/usr/bin/env python3

from major.PyMajor import PyMajor
from major.PyAyla import PyAyla
import os,sys
import time, datetime
import argparse

debug = 0

def debugprint(instr):
    if debug == 1:
        print(instr)
    
def snippet(myin,myout):
    debugprint(myin)
    return myout+myin+'\n'

parser = argparse.ArgumentParser()
parser.add_argument('groups', metavar='N', type=int, nargs='+', help='groups to OTA')
parser.add_argument('--wait', type=int, default=60)
parser.add_argument('--email', default=argparse.SUPPRESS)
parser.add_argument('--password', default=argparse.SUPPRESS)
args = parser.parse_args()

aylainfo = PyAyla(args)
groups = args.groups
waittime = args.wait

pymajorClient = PyMajor(aylainfo.env, aylainfo.login_email, aylainfo.login_password)
if pymajorClient:
    print('Client is instantiated for reboot')

message = ''
beforedev = {}

totaldevices = 0
for groupID in groups:
    thedevices = pymajorClient.get_group_details(groupID)
    if len(thedevices) != 0:
        totaldevices = totaldevices + len(thedevices)
        debugprint(thedevices)
        message = snippet('Number of devices in current group: {}'.format(len(thedevices)),message)
    else:
        print('No devices in this group: {}'.format(groupID))
        exit(1)

    # setup beforedev dictonary with { count : device }
    for count, device in enumerate(thedevices, start=totaldevices-len(thedevices)):
        details = pymajorClient.get_dsn_details(device)
        if details:
            beforedev[count] = details

print('Total devices: {}'.format(totaldevices))

# Initalize some stuff
allproperties = {}
beforeprop = {}
for key, value in beforedev.items():
    allproperties = pymajorClient.get_properties(value['id'])
    if not allproperties:
        # sleep 5 and try again, before failing
        time.sleep(5)
        allproperties = pymajorClient.get_properties(value['id'])

    debugprint(allproperties)

    # for each device save the all properties indexed by ['id']['<property name>']
    for count,each in enumerate(allproperties):
        beforeprop[value['id'],each['property']['name']] = each['property']

# deploy to thedevices - sleep waittime before exiting
for key, value in beforedev.items():
    PCBVer = beforeprop[value['id'],'GET_Main_PCB_FW_Version']['value']
    if value['connection_status'] == "Online" and PCBVer != "SharkM0.0.0":
        propID = beforeprop[value['id'],'SET_Robot_Volume_Control']['key']
        setprop = pymajorClient.set_property(propID, 25)
        if not setprop:
            foo = input("Problem with {}".format(value['id']))
    else:
        foo = input("{} is offline or PCBVer is {}".format(value['id'],PCBVer))

    time.sleep(1)

print('sleep after volume reset...')
time.sleep(waittime)
exit()
