#!/usr/bin/env python3

from major.PyMajor import PyMajor
from major.PyAyla import PyAyla
import os,sys
import time
import datetime
import argparse

debug = 0
jobsstarted = 0

def debugprint(instr):
    if debug == 1:
        print(instr)
    
def snippet(myin,myout):
    debugprint(myin)
    return myout+myin+'\n'
    

parser = argparse.ArgumentParser()
parser.add_argument('--name')
parser.add_argument('--email', default=argparse.SUPPRESS)
parser.add_argument('--password', default=argparse.SUPPRESS)
args = parser.parse_args()

aylainfo = PyAyla(args)
namestr = args.name

pymajorClient = PyMajor(aylainfo.env, aylainfo.login_email, aylainfo.login_password)
if pymajorClient:
    print('Client is instantiated for CheckJobs')

message = ''
    
myjobs = {}
otajobs = {}
alljobs = pymajorClient.get_ota_jobs('finished',namestr)
if not alljobs:
    print('No jobs returned')
else:
    print('Total jobs: {}'.format(alljobs[1]['total']))

start = 0
for each in alljobs:
    if each:
        for value in each['jobs']:
            otajobs[start] = value['job']
            start += 1

myjobsdetails = {}
#for key, value in otajobs.items():
#   myjobsdetails[value['id']] = pymajorClient.get_job_detail(value['id'])

for key, value in otajobs.items():
    if value['name'].startswith(namestr):
        if value['failed_count'] == 0:
            print("Deleting {} ID: {}".format(value['name'],value['id']))
            deletedjob = pymajorClient.delete_ota_job(value['id'])
        else:
            print('Job Name: {} - Status: {}, Device Count: {}, Failed: {}, Success: {}'.
                  format(value['name'],value['status'],value['device_count'],value['failed_count'],value['succeed_count']))
            
exit()
