#!/usr/bin/env python3

from major.PyMajor import PyMajor
from major.PyAyla import PyAyla
import os,sys
import time, datetime
import argparse

debug = 0

def debugprint(instr):
    if debug == 1:
        print(instr)
    
def snippet(myin,myout):
    debugprint(myin)
    return myout+myin+'\n'

parser = argparse.ArgumentParser()
parser.add_argument('devices', metavar='N', type=str, nargs='+', help='devices to Reset')
parser.add_argument('--goal', required=True)
parser.add_argument('--wait', type=int, default=60)
parser.add_argument('--email', default=argparse.SUPPRESS)
parser.add_argument('--password', default=argparse.SUPPRESS)
args = parser.parse_args()

aylainfo = PyAyla(args)
devices = args.devices
goal = args.goal
waittime = args.wait

pymajorClient = PyMajor(aylainfo.env, aylainfo.login_email, aylainfo.login_password)
if pymajorClient:
    debugprint('Client is instantiated for reboot')

message = ''
beforedev = {}

totaldevices = 0
count = 1

for macID in devices:
    # setup beforedev dictonary with { count : device }
    device = pymajorClient.get_device_byMAC(macID)
    while device['total'] == 0:
        time.sleep(30)
        device = pymajorClient.get_device_byMAC(macID)

    if device:
        details =  pymajorClient.get_dsn_details(device['devices'][0]['device']['dsn'])
        if details:
            beforedev[count] = details
            count += 1
        
debugprint('Total devices: {}'.format(count-1))

# Initalize some stuff
myproperties = {}
beforeprop = {}
time.sleep(waittime)
for key, value in beforedev.items():
    while not value['connection_status'] == "Online":
        time.sleep(15)
        details =  pymajorClient.get_dsn_details(value['dsn'])
        value = details
        
    if value['connection_status'] == "Online":
        myproperties = pymajorClient.get_properties(value['id'])
        if not myproperties:
            # sleep 5 and try again, before failing
            time.sleep(5)
            myproperties = pymajorClient.get_properties(value['id'])
            if not myproperties:
                if os.path.isfile('myslack.py'):
                    os.system("./myslack.py @johns problem with properties on {}".format(value['mac']))
                    exit()

        debugprint(myproperties)

        # for each device save the all properties indexed by ['id']['<property name>']
        for count,each in enumerate(myproperties):
            beforeprop[value['id'],each['property']['name']] = each['property']

        test = beforeprop[value['id'],'GET_Robot_Firmware_Version']['value']
        debugprint(test)
        wait = 1
        if not test.endswith(goal):
            if os.path.isfile('myslack.py'):
                os.system("./myslack.py @johns Incorrect version - {} - on {}".format(test,value['mac']))

        try:
            propID = beforeprop[value['id'],'SET_Reset_WiFi']['key']
            setprop = pymajorClient.set_property(propID, 1)
            if not setprop:
                if os.path.isfile('myslack.py'):
                    os.system("./myslack.py @johns WiFI reset problem with {}".format(value['mac']))
        except KeyError as e:
            if os.path.isfile('myslack.py'):
                os.system("./myslack.py @johns WiFi key problem with {}".format(value['mac']))
            pass
                    
        if os.path.isfile('myslack.py'):
            os.system("./myslack.py @johns Deploy complete on {}".format(value['mac']))
            
    else:
        if os.path.isfile('myslack.py'):
            os.system("./myslack.py @johns {} is offline".format(value['mac']))

    time.sleep(5)

exit()

