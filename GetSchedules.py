#!/usr/bin/env python3

from datetime import datetime, date, time
from major.PyMajor import PyMajor
from major.PyAyla import PyAyla
import dateutil.parser as dp
import time, datetime
import calendar
import argparse
import os, sys
import numpy as np

debug = 0

LAG = 30 # AYLA lag threshold in seconds
SCH_REPEAT = 4 # No.of times schedule repeats everyday.
LIMIT = 30 # No.of values of Op_mode to consider

RobotList = {
    "RV2019QF"  : "IQ",
    "RV2019QFA" : "IQ",
    "RV1000"    : "IQ",
    "RV1001"    : "IQ",
    "RV1000A"   : "IQ",
    "RV1001A"   : "IQ",
    "RV750N"    : "RB",
    "RV850"     : "RB",
    "RV871"     : "RB",
    "RV871Dev"  : "RB",
    "Spur+"     : "Spur+"
}

Error = {
    0 : 'alarmNone',
    1 : 'alarmReserved',
    2 : 'alarmSideBrush',
    3 : 'alarmFan',
    4 : 'alarmMainBrush',
    5 : 'alarmWheels',
    6 : 'alarmBumper',
    7 : 'alarmCliffSensor',
    8 : 'alarmNoBatteryOrFuse',
    9 : 'alarmNoDustBox',
    10 : 'alarmDrop',
    11 : 'alarmFrontWheelStuck',
    12 : 'alarmCharger',
    13 : 'alarmReserved1',
    14 : 'alarmMagStrip',
    15 : 'alarmReserved2',
    16 : 'alarmTopBumper',
    17 : 'alarmBatteryLife',
    18 : 'alarmWheelEncoder',
    19 : 'alarmAccelerometer',
    20 : 'alarmMainBrush2',
    21 : 'alarmNav',
    22 : 'alarmUL'
}

# Save and print only if debug enabled
def debugprint(instr):
    if debug == 1:
        f = open("logs\{}".format(log_filename), "a+")
        f.write(str(instr))
        print(instr)
        f.close()

# Save and print statements.
def sprint(instr):
    p.write("\n{}".format(str(instr)))
    print(instr)

# Prints the schedules. 
def recordSchedule(t, c_time, d_time, error=False):
    f=open("logs\Schedules_{}.txt".format(value['mac']), "a+")
    if error:
        text = "!!\tThere was {} error during the next schedule!\n".format(error)
    else:
        text = "++\tSchedule: started at {} | Cleaning time: {} | Docking time: {}\n".format(EpochToTime(t), c_time, d_time)
    f.write(text)
    f.close()
    sprint(text)

## utility functions ##
# Adds hours and wraps around 24h. 
def wrap(x):
    shift = int(24/SCH_REPEAT)
    shift = shift*60*60
    return (x+shift)%(24*60*60)

# Time of day as string to seconds
def toSeconds(str):
    return sum(x * int(t) for x, t in zip([3600, 60, 1], str.split(":"))) 

# Converts time from IOS8601 (str) to epochs (int)
def ISOtoEpoch(str):
    parsed_t = dp.parse(str)
    return int(parsed_t.timestamp())   

# Converts time from Epoch to readable time 
def EpochToTime(t): # Custom value instead of time.ctime(t)
    return time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(t))
    
# OG code from John Schectman
def snippet(myin,myout):
    debugprint(myin)
    return myout+myin+'\n'

# Returns epoch of last midnight UTC
def midnight(d):
    d = time.localtime(d)
    d = time.strftime('%Y-%m-%d ', d) + "00:00:00"
    d = time.mktime(time.strptime(d, "%Y-%m-%d %H:%M:%S"))
    return int(d)

# Returns True if given epoch time matches schedule day+time.
def is_Schedule(t, scheds, flag=0): 
    delta = t-midnight(t) #Find local midnight
    day = datetime.datetime.fromtimestamp(t).strftime("%A")

    for sched in scheds:
        if day==sched[0] and abs(sched[1]-delta)<=LAG:
            return True
        elif day==sched[0] and abs(sched[1]-delta)<=2*LAG:
            return True
    return False

# Hard coded
# Returns True if 't' between 12AM and 4:30AM
# Logic as of 13 Dec 2019
def isSilentReboot(t):
    delta = t-midnight(t)
    if delta>=0 and delta<4.5*3600:
        return True
    else:
        return False

# hard coded
# Checks IOT-1499 for RB or IQ
# Returns True/False and time of failure 
# Param: id, time of 1st '3', robot model
# Current logic: only check time. 
# todo: Check ChaSt, Battery Capacity 
def is_IOT1499(id, GCHAST, t): # Customize for RB and IQs
    if RobotList[value['oem_model']]=="IQ":
        T = t-90 # Z5 and Z8
    elif RobotList[value['oem_model']]=="RB":
        T = t-3600 # Y2 and Z1
    else:
        return False, t
    
    if is_Schedule(T, get_scheds(id)): # For Z5, Z8
        c_i = closest(T, [C[0] for C in GCHAST], lag=int(LAG/3), check='around')
        if c_i!=None and GCHAST[c_i][1]==0:
            return True, T
    else:
        if RobotList[value['oem_model']]=="IQ":
            T = t-180 # Z9
        elif RobotList[value['oem_model']]=="RB":
            T = t-4500 # Y1 and Y3
        else: # redundant
            return False, t
        
        if is_Schedule(T, get_scheds(id)): # For Z9
            c_i = closest(T, [C[0] for C in GCHAST], lag=int(LAG/3), check='around')
            if c_i!=None and GCHAST[c_i][1]==0:
                return True, T
    debugprint("\nNot IOT-1499. ")
    return False, t
    
# Returns index of value closest to v in ls. 
# check = 'before', 'after', or 'around
def closest(val, lst, lag=LAG, check='around'):
    lst = np.asarray(lst)
    nearest=None
    if check=='before':
        nearest = lst[lst<=val]
        if nearest.size>0:
            nearest = nearest.max()
        else:
            nearest=None
    elif check=='after':
        nearest = lst[lst>=val]
        if nearest.size>0:
            nearest = nearest.min()
        else:
            nearest=None
    else:
        try:
            nearest = min(lst, key=lambda x:abs(x-val))
        except:
            pass

    if nearest!=None:
        if abs(nearest-val) <= lag:
            debugprint("\n\tIndex Found {}s from given value".format(nearest-val))
        elif abs(nearest-val) <= 2*lag: # Checking a slightly wider delay time. If necessary. 
            debugprint("\n\tIndex Found (wider range) {}s from given value".format(nearest-val))
        else:
            debugprint("\n\tThe list does not contain value near enough to matter.")
            return None
        index = np.where(lst==nearest)
        if index[0].size>1:
            return int(index[0][0])
        else:
            return int(index[0])
    else:
        debugprint("\nNo value found within LAG time. Or the list passed to closest() was empty.")
        return None

# schedules (Active Schedule): Day and time as list of lists.
def get_scheds(id): # Param: value['id'], flag. 
    schedules = []  
    for each in pymajorClient.get_schedules(id):
        every = each['schedule']
        if every['active']:
            t = toSeconds(every['start_time_each_day'])
            for _ in range(0, int(SCH_REPEAT)):
                schedules.append([every['display_name'], t])
                t=wrap(t)
    debugprint("\nSchedules:\n{}".format(schedules))
    return schedules

# GOM (Operating Mode): Epoch time and value as a list of lists.
def get_GOM(id): # Param: value['id']
    GOM = [] 
    for each in pymajorClient.get_property_datapoints(beforeprop[id, 'GET_Operating_Mode']['key']):
        GOM.append([ISOtoEpoch(each['datapoint']['updated_at']), each['datapoint']['value']])
    debugprint("\nOperating Modes:\n{}".format(GOM))
    return GOM

# SOM (SET Operating Mode): Epoch time and value as a list of lists.
def get_SOM(id): # Param: value['id']
    SOM = [] 
    for each in pymajorClient.get_property_datapoints(beforeprop[id, 'SET_Operating_Mode']['key']):
        SOM.append([ISOtoEpoch(each['datapoint']['updated_at']), each['datapoint']['value']])
    debugprint("\nSET Operating Modes:\n{}".format(SOM))
    return SOM

# GEC (Error Code): Epoch time and error# as list of lists
def get_GEC(id): # Param: value['id']
    GEC = []
    for each in pymajorClient.get_property_datapoints(beforeprop[id, 'GET_Error_Code']['key']):
        GEC.append([ISOtoEpoch(each['datapoint']['updated_at']), each['datapoint']['value']])
    debugprint("\nError Codes:\n{}".format(GEC))
    return GEC

# GCS (Cleaning Statistics): List of epoch times.
def get_GCS(id): # Param: value['id']
    GCS = []
    for each in pymajorClient.get_property_datapoints(beforeprop[id, 'GET_Cleaning_Statistics']['key']):
        GCS.append(ISOtoEpoch(each['datapoint']['updated_at']))
    debugprint("\nCleaning Statistics:\n{}".format(GCS))
    return GCS

# GChaSt (Charging Status): Epoch time and value as a list of lists.
def get_GChaSt(id): # Param: value['id']
    GChaSt = []
    for each in pymajorClient.get_property_datapoints(beforeprop[id, 'GET_Charging_Status']['key']):
        GChaSt.append([ISOtoEpoch(each['datapoint']['updated_at']), each['datapoint']['value']])
    debugprint("\nCharging Status:\n{}".format(GChaSt))
    return GChaSt

# converts seconds to h, m, s
def convert(t):
    m, s = divmod(t, 60)
    h, m = divmod(m, 60)
    if h!=0:
        T=str(h) + 'h ' + str(m) + 'm ' + str(s) + 's'
    elif m!=0:
        T=str(m) + 'm ' + str(s) + 's'
    else:
        T=str(s) + 's'
    return T

# Sets given list of flags to given value. Ignores rest.
def setflag(true_list, value):
    for key in true_list:
        flag[key]=value
    debugprint("Flags set are {}".format(true_list))

# Currently not in use
# Returns True only if the flags in param are set.
def only(s=['']):
    # check if only the flags in param are set in flag dictionary.
    for key in s:
        if flag[key]==True:
            debugprint("'{}' flag was True.".format(key))
            sprint("'{}' flag was True.".format(key))
        elif key=='mission':
            pass
        else:
            debugprint("\nERROR! '{}' flag should NOT be set but it is!".format(key))
            sprint("\nERROR! '{}' flag should NOT be set but it is!".format(key))
            return False
    return True

# Returns True if time in param is at 1AM/PM
def is_AylaPing(t):
    t = time.localtime(t)
    if time.strftime("%H", t)=="01" or time.strftime("%H", t)=="13":
        return True # Assume ping if 't' at 1AM/PM
    return False 
    
# Accoutns for: 
    # Schedules, Commands, Errors, Redocks,
    # Ayla pings, IOT-1499, Silent Reboot
def analyse(id):
    # State dictionary
    global flag
    flag = {
        'paused'    : False,
        'cleaning'  : False,
        'docking'   : False,
        'docked'    : False, 
        'schedule'  : False,
        'error'     : False, # Only when paused and error not cleared.
        'redock'    : False, # redock or send command to dock
        'IOT1499'   : False  # If IOT-1499
    }

    # Initialise statistics
    clean_start = -1
    dock_start = -1
    c_time = '0s'
    d_time = '0s'

    op_mode = get_GOM(id)
    op_mode = op_mode[-LIMIT:] 
    # Set initial state
    t, m = op_mode[0]
    if m==0:
        setflag(['paused'], True)
        sprint("\n\tRobot is currently Paused.")
    elif m==2:
        setflag(['cleaning'], True)
        sprint("\n\tRobot is currently Cleaning.")
        clean_start=t
    elif m==3:
        VCHAST = get_GChaSt(id)
        ch_i = closest(t, [Ch[0] for Ch in VCHAST], lag=LAG, check='before')
        if ch_i!=None and VCHAST[ch_i][1]==1:
            setflag(['docked'], True)
            sprint("\n\tRobot is currently Docked.")
        else:
            setflag(['docking'], True)
            sprint("\n\tRobot is currently Docking.")
            dock_start=t
    debugprint("Setting initial state to {}".format(m))

    # For each [epoch, value] in op_mode
    for t, m in op_mode[1:]:
        # todo: Evac and Resume, Recharge and resume.

        GEC = get_GEC(id)
        e_i = closest(t, [E[0] for E in GEC], lag=int(LAG/3), check='around')
        SOM = get_SOM(id)
        s_i = closest(t, [S[0] for S in SOM], lag=LAG, check='around')
        s_i_b = closest(t, [S[0] for S in SOM], lag=LAG, check='before')
        s_i_a = closest(t, [S[0] for S in SOM], lag=LAG, check='after')
        GCHAST = get_GChaSt(id)
        c_i = closest(t, [C[0] for C in GCHAST], lag=int(LAG/3), check='around')
                
        debugprint("\nTime: {}, Operating Mode: {}".format(EpochToTime(t), m))
        debugprint("\nFlags: {}".format(flag))
                
        if m==0: # incoming pause mode
            #if currently cleaning or docking
            if flag['cleaning'] == True or flag['docking']==True:
                if e_i!=None: # if error
                    sprint("\tRobot paused at {} due to Error: {}.".format(EpochToTime(t), Error[GEC[e_i][1]]))
                    setflag(['error'], True)
                    if flag['schedule']==True:
                        sprint("x\tSCHEDULE RUN WITH ERROR(S)!")
                        recordSchedule(t, c_time, d_time, error=Error[GEC[e_i][1]])
                elif s_i_a!=None: # if button command
                    sprint("\tUser paused robot from button press at {}".format(EpochToTime(t)))
                elif s_i_b!=None: # if app command
                    sprint("\tUser paused robot from app at {}".format(EpochToTime(t)))    
                else: # If any other reason.
                    sprint("\n\tNot sure why, but Robot went to Pause mode at {}".format(EpochToTime(t)))
                
                # Checking for times.
                if flag['cleaning']==True:
                    c_time = convert(t-clean_start)
                elif flag['docking']==True:
                    d_time = convert(t-dock_start)
                
                # Set States
                setflag(['cleaning', 'docking'], False) # Not clearing schedule to account get better results.
                setflag(['paused'], True)
                
            # if currently docked
            elif flag['docked']==True:
                if s_i_a!=None and SOM[s_i_a][1]==0: # if robot sent command to ayla
                    sprint("\tRobot is off the dock. Time: {}".format(EpochToTime(t)))
                    setflag(['docked'], False)
                    setflag(['paused'], True)
                elif e_i!=None: # If error (although not sure what could cause this condition)
                    sprint("\tRobot paused at {} due to Error: {}.".format(EpochToTime(t), Error[GEC[e_i][1]]))
                    setflag(['error'], True)
                else: # If any other reason
                    #if ???:
                    #    sprint("\tRobot is off the dock, but SOM was not updated within {}s. Time: {}".format(2*LAG, EpochToTime(t)))
                    sprint("\tRobot went to pause mode. Possibly incorrect docking. Check if NOT docked immediately after this. Time: {}".format(EpochToTime(t)))

                setflag(['docked'], False)        
                setflag(['paused'], True)
            
            # if currently paused or in error state
            elif flag['paused']==True:    
                if flag['error']==True:
                    if e_i!=None and is_Schedule(t, get_scheds(t)):
                        sprint("\tError cleared by schedule run command at {}. Schedule fail.".format(EpochToTime(t)))
                        recordSchedule(clean_start, c_time, d_time)    
                        sprint("\n")
                    elif e_i!=None: 
                        if GEC[e_i][1]==0:
                            sprint("\talarmNone. Clearing error. Time: {}".format(EpochToTime(t)))
                            setflag(['error'], False)
                        else:
                            sprint("\tRobot had Error: {} while in error state. Time: {}.".format(Error[GEC[e_i][1]], EpochToTime(t)))
                            setflag(['error'], True)
                    else:
                        sprint("\tError cleared at {} by user command. (app or button)".format(EpochToTime(t)))
                        setflag(['error'], False)
                elif e_i!=None: 
                    sprint("\tRobot had Error: {} while in pause state. Time: {}.".format(Error[GEC[e_i][1]], EpochToTime(t)))
                    setflag(['error'], True)
                else:
                    sprint("\tSecond paused value at {}.".format(EpochToTime(t)))
                    setflag(['error'], False)
                setflag(['paused'], True)
                
        elif m==2:
            # cross verify with : schedules, SET_Operating_Mode, 
            # if docked/paused --> start mission
                # Does NOT handle Recharge resume !
            if flag['docked']==True or (flag['paused']==True and flag['error']==False):
                #print(is_Schedule(t, get_scheds(id)))
                if is_Schedule(t, get_scheds(id)): # If schedule
                    sprint("\n>>\tRobot started cleaning on schedule at {}".format(EpochToTime(t)))
                    setflag(['docked', 'error', 'redock'], False) # Not docked. Cleared error.
                    setflag(['schedule'], True) 
                elif s_i!=None and SOM[s_i][1]==2: # If command. Need app/button check?
                    sprint("\n\tUser set robot to clean at {}".format(EpochToTime(t)))
                    setflag(['docked', 'error', 'redock', 'schedule'], False) # Not schedule
                else: # If anything else
                    sprint("\n\tERROR: Robot started due to unknown reason! Time: {}".format(EpochToTime(t)))
                    setflag(['docked', 'error', 'redock', 'schedule'], False)
                
                setflag(['cleaning'], True)
                dock_start = -1
                d_time = c_time = "0s"
                clean_start=t
                    
            elif flag['docking']==True:
                if s_i!=None and SOM[s_i][1]==2:  
                    sprint("\tUser interrupted docking and set robot to clean through app at {}".format(EpochToTime(t)))
                    sprint("\t\tResetting all cleaning times.")
                else:
                    sprint("CORNER CASE! THIS STATE IN NOT EXPECTED! Time: {}".format(EpochToTime(t)))
                
                setflag(['docking', 'schedule', 'redock'], False)
                setflag(['cleaning'], True)
                c_time = d_time = "0s"
                dock_start = -1
                clean_start=t

            elif flag['paused']==True:
                if flag['error']==True: # different for IQ and RB
                    if RobotList[value['oem_model']]=="IQ":
                        sprint("\tUser gave clean command with Error. Error cleared. Robot Paused. Time: {}".format(EpochToTime(t)))
                        setflag(['error'], False)
                        setflag(['paused'], True)
                    elif RobotList[value['oem_model']]=="RB": 
                        # RB can go from Error to clean/dock without Pause mode.
                        if is_Schedule(t, get_scheds(id)): 
                            if flag['schedule']==True:
                                recordSchedule(t, c_time, d_time)
                            sprint("\n!!\tRB Robot started cleaning on schedule, from Error, at {}".format(EpochToTime(t)))
                            setflag(['paused', 'error', 'redock'], False)
                            setflag(['cleaning', 'schedule'], True)
                            
                            dock_start=-1
                            d_time = c_time = "0s"
                            clean_start=t
                    
                elif is_Schedule(t, get_scheds(id)): 
                    sprint("\n\tRobot started cleaning on schedule, from Pause, at {}".format(EpochToTime(t)))
                    if flag['schedule']==True:
                        recordSchedule(t, c_time, d_time)
                    setflag(['paused', 'error', 'redock'], False)
                    setflag(['cleaning', 'schedule'], True)
                    
                    dock_start=-1
                    d_time = c_time = "0s"
                    clean_start=t
                    
                elif s_i!=None and SOM[s_i][1]==2: # redundant
                    sprint("\tRobot started cleaning by user command, from paused state at {}".format(EpochToTime(t)))

                    setflag(['paused', 'error', 'schedule', 'redock'], False)
                    setflag(['cleaning'], True)

                    dock_start=-1
                    d_time = c_time = "0s"
                    clean_start = t
            
            elif flag['cleaning']==True: # network disconnect and connect while cleaning # todo
                sprint("\n! Check: Extra clean value! Time: {}".format(EpochToTime(t)))
                setflag(['cleaning'], True)
            
        elif m==3:
            # if cleaning --> docking
            if flag['cleaning']==True:
                if s_i!=None and SOM[s_i][1]==3:
                    sprint("\tRobot given command to dock at {}.".format(EpochToTime(t)))
                elif s_i_a!=None and SOM[s_i_a][1]==3:
                    sprint("\tUser sent robot to dock from the app. Time: {}".format(EpochToTime(t)))
                else: # Finished cleaning, or button press
                    sprint("\tRobot switched to dock mode at {}".format(EpochToTime(t)))
                
                setflag(['cleaning', 'redock'], False)
                setflag(['docking'], True)
                
                c_time = convert(t-clean_start)
                dock_start = t
                d_time = '0s'
                    
            # if docking --> docked
            # cross verify with : Charging status
            elif flag['docking']==True:
                if c_i!=None and GCHAST[c_i][1]==1: 
                    sprint("\tRobot finished docking and started charging at {}.".format(EpochToTime(t)))
                    d_time = convert(t-dock_start)
                    sprint("--\tMission complete. Cleaning time: {}. Docking Time: {}".format(c_time, d_time))
                    
                    if flag['schedule']==True:
                        recordSchedule(clean_start, c_time, d_time)
                        setflag(['schedule'], False)  
                    
                    c_time = '0s'
                    d_time = '0s'
                    setflag(['IOT1499'], False)
                else:
                    sprint("x\tPossibly incorrect docking. Check this if next value is NOT pause state. Time: {}".format(EpochToTime(t)))
                
                setflag(['docking', 'redock'], False)
                setflag(['docked'], True)


            # if docked --> check ayla ping/ check reboot/ check redock
            # cross verify with : connection history(reboot), charging status(redock)            
            elif flag['docked']: # start working here
                if RobotList[value['oem_model']]=="IQ" and isSilentReboot(t): # also check Ch St
                    sprint("\n\tThis robot performed a silent reboot at {}".format(EpochToTime(t)))
                elif s_i!=None:
                    val, sched_time = is_IOT1499(id, GCHAST, t)
                    if val and not c_i:
                        sprint("\nx\tIssue: *Possibly* IOT-1499. Cleaning started at {}. Time: {}".format(EpochToTime(sched_time), EpochToTime(t)))     
                        
                        setflag(['docked', 'cleaning', 'redock'], False)
                        setflag(['docking', 'schedule', 'IOT1499'], True)

                        clean_start = sched_time
                        c_time = convert(t-clean_start)
                        dock_start = t
                        d_time = '0s'
                    elif is_Schedule(t, get_scheds(id)):
                        if e_i!=None and GEC[e_i][1]!=0:
                            sprint("\n\tThere was an error starting schedule. Error: {}".format(Error[GEC[e_i][1]]))  
                            setflag(['error'], True)

                            if c_i: # if 'None', Robot is already known to be docked. If '0', robot is not charging, thus error stays. 
                                sprint("\tRobot is on the dock and charging. Clearing all errors.")
                                setflag(['error'], False)
                        else:
                            sprint("\n\tThe robot failed a schedule. No error reported. Time: {}".format(EpochToTime(t)))
                        
                    elif e_i!=None: # if error and no schedule
                        if SOM[s_i][1]!=3:
                            sprint("\nCommand {} did not work due to Error #{}".format(SOM[s_i][1], GEC[e_i][1]))
                    elif SOM[s_i][1]==3: # and GCHAST[c_i][1]==1: obviously 1 since docked
                        if is_AylaPing(t):
                            sprint("\tAyla ping (Extra '3') at time: {}".format(EpochToTime(t)))
                        else:
                            sprint("\tExtra '3'. Possibly an acknowledgement. Time: {}".format(EpochToTime(t)))
                    else:
                        sprint("Not IOT-1499. Not schedule. Not missed command. Not Ayla ping. Please check. Time: {}".format(EpochToTime(t)))                        
                else:
                    sprint("--- extra '3' value. Unknown complication. Please check. Time: {} ----".format(EpochToTime(t))) 
                      
            # if paused --> redock / docking
            # cross verify with : SET Operating Mode 
            elif flag['paused']==True:
                if c_i!=None:
                    if s_i!=None and SOM[s_i][1]==3 and GCHAST[c_i][1]==0:
                        sprint("\tCommand given to robot to return to dock. Time:{}".format(EpochToTime(t)))
                        
                        setflag(['paused'], False)
                        setflag(['redock', 'docking'], True)
                        
                        dock_start=t
                
                    elif GCHAST[c_i][1]==1:
                        sprint("--\tRobot was placed on dock. (Or wiggled itself in.) Robot charging. Time: {}\n".format(EpochToTime(t)))
                        if flag['schedule']==True:
                            recordSchedule(clean_start, c_time, d_time)
                            setflag(['schedule'], False)  
                        setflag(['docked'], True)
                        setflag(['paused', 'error', 'docking', 'redock', 'cleaning', 'IOT1499'], False)   
                        
                        c_time = '0s'
                        d_time = '0s'
                        clean_start = -1
                        dock_start = -1 
                elif c_i==None:
                    sprint("\tRobot was not on dock.(Maybe just boot up.) Robot given command to dock. Time: {}".format(EpochToTime(t)))
                    
                    setflag(['paused'], False)
                    setflag(['redock', 'docking'], True)
                        
                    dock_start=t
                else:
                    print(GCHAST)
                    sprint("!! I have no idea what is happening at this point. Time: {}".format(EpochToTime(t)))
            # Docked/Docking
            
        elif m==-63:
            # cross verify with : Battery Capacity < 80
            sprint("\tOp_mode value -63. Redock! Time: {}".format(EpochToTime(t)))
            if flag['paused']==True:
                setflag(['redock'], True)
            # redock for RBR. Just set flag. 

    sprint("{} Done.".format(value['product_name']))
    return None


## Execution starts here ##
# Parsing arguments
parser = argparse.ArgumentParser()
parser.add_argument('devices', metavar='N', type=str, nargs='+', help='devices to Reset')
parser.add_argument('--email', default=argparse.SUPPRESS)
parser.add_argument('--password', default=argparse.SUPPRESS)
args = parser.parse_args()


aylainfo = PyAyla(args)
devices = args.devices

# todo: If device list empty, check device list file.

pymajorClient = PyMajor(aylainfo.env, aylainfo.login_email, aylainfo.login_password)
if pymajorClient:
    debugprint('Client is instantiated for reboot')

message = ''
beforedev = {}

totaldevices = 0
count = 1

for macID in devices:
    # setup beforedev dictonary with { count : device }
    device = pymajorClient.get_device_byMAC(macID)
    while device['total'] == 0:
        time.sleep(30)
        device = pymajorClient.get_device_byMAC(macID)

    if device:
        details =  pymajorClient.get_dsn_details(device['devices'][0]['device']['dsn'])
        if details:
            beforedev[count] = details
            count += 1
        
debugprint('Total devices: {}'.format(count-1))

# Initalize some stuff
myproperties = {}
beforeprop = {}

for key, value in beforedev.items():
    while not value['connection_status'] == "Online":
        time.sleep(15)
        details =  pymajorClient.get_dsn_details(value['dsn'])
        value = details
        
    if value['connection_status'] == "Online":
        myproperties = pymajorClient.get_properties(value['id'])
        if not myproperties:
            # sleep 5 and try again, before failing
            time.sleep(5)
            myproperties = pymajorClient.get_properties(value['id'])
            if not myproperties:
                if os.path.isfile('myslack.py'):
                    os.system("./myslack.py @johns problem with properties on {}".format(value['mac']))
                    exit()

        # for each device save the all properties indexed by ['id']['<property name>']
        for count,each in enumerate(myproperties):
            beforeprop[value['id'],each['property']['name']] = each['property']

        # Routine for Operating mode pattern analysis.
        p = open("logs\op_{}_{}.txt".format(value['mac'], os.environ.get('AYLAENV')), "a+")
        log_filename = "runtime_log_{}_{}.txt".format(value['mac'], os.environ.get('AYLAENV'))

        sprint("\n---------------")
        sprint("Current time\t: {}".format(time.asctime(time.localtime())))
        sprint("Robot Name\t: {}.".format(value['product_name']))
        sprint("Model No.\t: {}".format(value['oem_model']))
        sprint("MAC Address\t: {}".format(value['mac']))
        if RobotList[value['oem_model']]=="IQ":
            sprint("Main PCB FW\t: {}".format(beforeprop[value['id'], 'GET_Main_PCB_FW_Version']['value']))
            sprint("Nav App ver.\t: {}".format(beforeprop[value['id'], 'GET_Nav_Module_App_Version']['value']))
            sprint("SCM FW ver.\t: {}".format(beforeprop[value['id'], 'GET_SCM_FW_Version']['value']))
        elif RobotList[value['oem_model']]=="RB":
            sprint("Main PCB FW ver.: {}".format(beforeprop[value['id'], 'GET_Main_PCB_FW_Version']['value']))
            sprint("WiFi FW ver.\t: {}".format(beforeprop[value['id'], 'GET_WiFi_FW_Version']['value']))
        sprint("---------------")
        analyse(value['id'])
        p.close()
exit()


# Command for daily schedule tests:
'''
set AYLAENV=field
python GetSchedules.py c0835952ce26 80c5f289ecfc c0835952fd03 c08359594606 18c8e72ec968 18c8e72f446b 18c8e72ecb48
'''

# Dev robot for testing. 
# 18c8e72ef751 

# Field robots in schedule test
# Y1 c0835952ce26
# Y2 80c5f289ecfc
# Y3 c0835952fd03
# Z1 c08359594606
# Z5 18c8e72ec968
# Z8 18c8e72f446b
# Z9 18c8e72ecb48 
'''
Raghav's ayla login credentials
[dev]
ayla_email = rkrishna@sharkninja.com
ayla_password = gAAAAABduy2tf3MBvlMLEROUKVeL5HS0SifCP8AyDQutjcbwtoFv5mMMZhPVVvStbUNaasmfwll4QHDiESVb-09lZ3PFEMPWsA==

[field]
ayla_email = rharikrishna@sharkninja.com
ayla_password = gAAAAABd3UdLOBhTFME7bFdiXtmS8dygaymJRNT48fWuIQO-Osp9UAn2uHeSJUxVoa3Cda5PMfT1W-GNUDztPCYLZikZG2_QFA== 
'''