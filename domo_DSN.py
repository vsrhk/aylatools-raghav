#!/usr/bin/env python3

import logging
import csv
from pydomo import Domo
from pydomo.datasets import DataSetRequest, Schema, Column, ColumnType, Policy, UpdateMethod
from pydomo.datasets import PolicyFilter, FilterOperator, PolicyType, Sorting

# Build an SDK configuration
client_id = '42151310-8af1-458e-8e0d-7c1f4c3fae16'
client_secret = '68b3e2885364e67e42b0b1dcb1f6d1837a998a3d2051c289eafc9e8ce4027ded'
api_host = 'api.domo.com'
mydataset = 'c5d75bb3-bdcd-4a04-869e-a44f5f8d8de0'
# Configure the logger
handler = logging.StreamHandler()
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logging.getLogger().addHandler(handler)

# Create an instance of the SDK Client
domo = Domo(client_id, client_secret, logger_name='foo', log_level=logging.INFO, api_host=api_host)

datasets = domo.datasets

# List DataSets
#dataset_list = list(datasets.list(sort=Sorting.NAME))
#print(dataset_list)
#exit()

#update_schema = DataSetRequest()
#update_schema.schema = Schema([Column(ColumnType.STRING, 'DSN'),
#                     Column(ColumnType.STRING, 'MAC'),
#                     Column(ColumnType.STRING, 'UserEmail'),
#                     Column(ColumnType.STRING, 'OEM Model')])
#updated_dataset = datasets.update(mydataset, update_schema)

update = datasets.data_import_from_file(mydataset, "domo.csv",
                                        UpdateMethod.REPLACE)
print(update)
exit()
# alldata = datasets.data_export(dataset_list[29]['id'],True)
# print(dataset_list[29])
# foo = input("Continue?")

# headers = alldata.split('\n',1)
# lessdata = headers[1].split('\n',2000000)
# orgdata = csv.reader(lessdata,headers[0].split(','))
#robot = [None] * len(lessdata)
#count = 0
#for row in orgdata:
#    if row:
#        if row[4] == "RV1000" or row[4] == "RV1000A":
#            robot[count] = row
#            count += 1

#with open ('robots.csv', 'w', newline='') as csvfile:
#    spamwriter = csv.writer(csvfile, delimiter=',')
#    for each in range(0, count):
#        spamwriter.writerow(robot[each])


# Define a DataSet Schema
dsr = DataSetRequest()
dsr.name = 'JSchectmanRobotUpdates'
dsr.description = 'RobotUpdates'
dsr.schema = Schema([Column(ColumnType.STRING, 'DSN'),
                     Column(ColumnType.DECIMAL, 'DSN Registration Count'),
                     Column(ColumnType.DATETIME, 'First Registration Date'),
                     Column(ColumnType.STRING, 'Registration UUID'),
                     Column(ColumnType.STRING, 'Registered Model'),
                     Column(ColumnType.DOUBLE, 'First Lat'),
                     Column(ColumnType.DOUBLE, 'First Long'),
                     Column(ColumnType.DOUBLE, 'Final Lat'),
                     Column(ColumnType.DOUBLE, 'Final Long'),
                     Column(ColumnType.DECIMAL, 'Location Count')])
dataset = datasets.create(dsr)
print("Created DataSet: {}".format(dataset['id']))

